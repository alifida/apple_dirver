//
//  TripDetailVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 25/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import PINRemoteImage
import MarqueeLabel

class TripDetailVC: UIViewController {

    @IBOutlet weak var mapScreenImg: UIImageView!
   
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var sourceLbl: MarqueeLabel!
    
    @IBOutlet weak var desgnationLbl: MarqueeLabel!
    
    @IBOutlet weak var totalBill: UILabel!
    
    @IBOutlet weak var vehcileNameLbl: UILabel!
    @IBOutlet weak var totalCostLbl: UILabel!
    @IBOutlet weak var rideStatus: UILabel!
    
    @IBOutlet weak var paymentTitle: UILabel!
    
    @IBOutlet weak var cashTypeLbl: UILabel!
    @IBOutlet weak var rideCashLbl: UILabel!
    @IBOutlet weak var rideFareLbl: UILabel!
  
    
    @IBOutlet weak var rideFarePrice: UILabel!
    
    @IBOutlet weak var baseFareLbl: UILabel!
    
    @IBOutlet weak var baseFarePrice: UILabel!
    
    @IBOutlet weak var timeFare: UILabel!
    @IBOutlet weak var timeFarePrice: UILabel!
   
    @IBOutlet weak var pickupFare: UILabel!
    
    @IBOutlet weak var pichupFarePrice: UILabel!
    
    @IBOutlet weak var accessFare: UILabel!
   
    @IBOutlet weak var accessFarePrice: UILabel!
    
    @IBOutlet weak var cancelFare: UILabel!
    @IBOutlet weak var cancelFarePrice: UILabel!
    @IBOutlet weak var subtotalFare: UILabel!
    @IBOutlet weak var subtotalPriceFare: UILabel!
    
    @IBOutlet weak var RideFareView: UIView!
    @IBOutlet weak var baseFareView: UIView!
    @IBOutlet weak var timeFareView: UIView!
    @IBOutlet weak var pickupFareView: UIView!
    @IBOutlet weak var accessFareView: UIView!
    @IBOutlet weak var cancellationView: UIView!
    @IBOutlet weak var subtotalView: UIView!
    
    //initilaze Variable
    let Localize : Localizations = Localizations.instance
    var tripDetailVM = YourTripsVM()
    var tripId : String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let tripid : String = self.tripId{
           getrideDetail(tripId: tripId)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.tripDetailVM = YourTripsVM(dataService: ApiRoot())
        self.setupLang()
        self.setupView()
        self.setupAction()
    }
    
    func setupView(){
        self.mapScreenImg.layer.cornerRadius = 10
    }
    func setupAction(){
        
    }
    func setupLang(){
        self.rideFareLbl.text = Localize.stringForKey(key: "ride_fare")
        self.baseFareLbl.text = Localize.stringForKey(key: "base_fare")
        self.timeFare.text = Localize.stringForKey(key: "time_fare")
        self.pickupFare.text = Localize.stringForKey(key: "pickup_charge")
        self.accessFare.text = Localize.stringForKey(key: "access_fee")
        self.cancelFare.text = Localize.stringForKey(key: "cancellation_fee")
        self.subtotalFare.text = Localize.stringForKey(key: "subtotal")
        self.totalBill.text = Localize.stringForKey(key: "total_bill")
        self.paymentTitle.text = Localize.stringForKey(key: "payment_method")
    }

    class func initWithStory()->TripDetailVC{
        let vc = UIStoryboard.init(name: "YourTrips", bundle: Bundle.main).instantiateViewController(withIdentifier: "TripDetailVC") as! TripDetailVC
        return vc
    }

}
//Api calling
extension TripDetailVC{
    func getrideDetail(tripId : String){
        self.tripDetailVM.getRideDetail(view: self.view, tripId: tripId)
        
        self.tripDetailVM.successTripDetail = {
            print("SuccessTripData",self.tripDetailVM.yourTripDetail)
            self.setData(data: self.tripDetailVM.yourTripDetail ?? TripDetailModel())
        }
    }
    
    func setData(data : TripDetailModel){
        self.userNameLbl.text = data.profileDetail.fname
      
        var urls : String = data.profileDetail.profile ?? String()
        self.userImg?.pin_setImage(from: URL(string: urls))
        
        var url : String = data.Mapurl ?? String()
        self.mapScreenImg?.pin_setImage(from: URL(string: url))
       
        if data.tripDetail.status == "Finished"{
            self.sourceLbl.text = data.tripDetail.adsp.from
            self.desgnationLbl.text = data.tripDetail.adsp.to
        }else if data.tripDetail.status == "canceled"{
            self.sourceLbl.text = data.tripDetail.dsp.start
            self.desgnationLbl.text = data.tripDetail.dsp.end
        }
        
        self.vehcileNameLbl.text = data.tripDetail.vehicle
        self.cashTypeLbl.text = data.tripDetail.paymentMode
        self.rideCashLbl.text = data.tripDetail.paymentSts
        self.rideStatus.text = data.tripDetail.status
        self.totalCostLbl.text = decimalDataString(data: data.tripDetail.acsp.cost.description)
        
        self.rideFarePrice.text = decimalDataString(data: data.tripDetail.acsp.actualcost.description)
        self.baseFarePrice.text = decimalDataString(data: data.tripDetail.acsp.base.description)
        self.timeFarePrice.text = decimalDataString(data: data.tripDetail.acsp.timefare.description)
        self.pichupFarePrice.text = decimalDataString(data: data.tripDetail.acsp.conveyance.description)
        self.accessFarePrice.text = decimalDataString(data: data.tripDetail.acsp.tax.description)
        self.cancelFarePrice.text = decimalDataString(data: data.tripDetail.acsp.oldBalance.description)
        self.subtotalPriceFare.text = decimalDataString(data: data.tripDetail.acsp.cost.description)
        
        
    }
}
