//
//  SignupVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 30/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import CountryPickerView
import JWTDecode
import GoogleSignIn
import FBSDKLoginKit

class SignupVC: UIViewController , UITextFieldDelegate{
    
    //UI Declaraction
    //Textfield
    @IBOutlet weak var fireNameTXF: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lastNameTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var emailAddressTXF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTFX: SkyFloatingLabelTextField!
    
    @IBOutlet weak var mobileNumTXF: SkyFloatingLabelTextField!
    @IBOutlet weak var ccTXF: CountryPickerView!
    @IBOutlet weak var referralTXF: SkyFloatingLabelTextField!
    
    @IBOutlet weak var addressTXF: SkyFloatingLabelTextField!
    
    //UI ImageView
    @IBOutlet weak var checkImg: UIImageView!
    
    //UILAbel
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var signUpLbl: UILabel!
    @IBOutlet weak var acceptTermsCond: UILabel!
    @IBOutlet weak var loginLbl: UILabel!
    
    //UI button
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var googleImg : UIImageView!
    @IBOutlet weak var facebookImg : UIImageView!
    
    //Variable Declaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var countryCode : String = ""
    var loginVM = LoginSignupVM()
    let otpView = OTPView.getView
    var countryData : Country?
    var mobileNum : String = ""
    
    //Firebase Object
    let FBCONNECT = FireBaseconnection.instanse
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.loginVM = LoginSignupVM(view: self.view, dataService: ApiRoot())
        self.setupData()
        self.setupView()
        self.setupLang()
        self.setupAction()
        self.setupCountryPicker()
        self.setupTextFieldDelegate()
        self.configureGoogleSignIn()
    }
    
    class func initWithStory()->SignupVC{
        let vc = UIStoryboard.init(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        return vc
    }
}

//Mark:- Gentral Function
extension SignupVC {
    
    func setupTextFieldDelegate(){
        self.fireNameTXF.delegate = self
        self.lastNameTXT.delegate = self
        self.emailAddressTXF.delegate = self
        self.passwordTFX.delegate = self
        self.mobileNumTXF.delegate = self
        self.referralTXF.delegate = self
        self.addressTXF.delegate = self
    }
    
    // data setup
    func setupData(){
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            self.countryCode = countryCode
        }
    }
    
    // View Set up
    func setupView() {
        self.ccTXF.setCountryByCode(Constant.countryCode.uppercased())
        self.countryCode = Constant.phoneCode
        
        self.submitBtn.roundeCornorBorder = 20
        
        self.fireNameTXF.keyboardType = UIKeyboardType.alphabet
        self.lastNameTXT.keyboardType = UIKeyboardType.alphabet
        self.emailAddressTXF.keyboardType = UIKeyboardType.emailAddress
        self.mobileNumTXF.keyboardType = UIKeyboardType.numberPad
        self.referralTXF.keyboardType = UIKeyboardType.alphabet
        self.addressTXF.keyboardType = UIKeyboardType.alphabet
        self.passwordTFX.isSecureTextEntry = true
        
        //setup color
        self.submitBtn.backgroundColor = UIColor(named: "AppColor")
        self.acceptTermsCond.textColor = UIColor.black
        
        welcomeLbl.text = Localize.stringForKey(key: "Welcome")
        signUpLbl.text = Localize.stringForKey(key: "SIGN_up")
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.darkGray]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor : UIColor.AppColors]
        
        let attributedString1 = NSMutableAttributedString(string:"\(Localize.stringForKey(key: "existing_user")) " , attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:Localize.stringForKey(key: "SIGN_IN"), attributes:attrs2)
        
        attributedString1.append(attributedString2)
        self.loginLbl.attributedText = attributedString1
        
        //setup color
        self.submitBtn.backgroundColor = UIColor(named: "AppColor")
        self.acceptTermsCond.textColor = UIColor.black
        
        //        self.ccTXF.text = Constant.phoneCode
    }
    
    func setupLang(){
        self.fireNameTXF.placeholder = Localize.stringForKey(key: "first_name")
        self.fireNameTXF.title = Localize.stringForKey(key: "first_name")
        
        self.lastNameTXT.placeholder = Localize.stringForKey(key: "last_name")
        self.lastNameTXT.title = Localize.stringForKey(key: "last_name")
        
        self.emailAddressTXF.placeholder = Localize.stringForKey(key: "email_address")
        self.emailAddressTXF.title = Localize.stringForKey(key: "email_address")
        
        self.passwordTFX.placeholder = Localize.stringForKey(key: "password")
        self.passwordTFX.title = Localize.stringForKey(key: "password")
        
        self.mobileNumTXF.placeholder = Localize.stringForKey(key: "mobile_num")
        self.mobileNumTXF.title = Localize.stringForKey(key: "mobile_num")
        
        self.referralTXF.placeholder = Localize.stringForKey(key: "referral")
        self.referralTXF.title = Localize.stringForKey(key: "referral")
        
        self.addressTXF.placeholder = Localize.stringForKey(key: "address")
       self.addressTXF.title = Localize.stringForKey(key: "address")
        
        
        self.acceptTermsCond.text = Localize.stringForKey(key: "tc_pp")
        
        self.submitBtn.setTitle(Localize.stringForKey(key: "submit"), for: .normal)
    }
    
    // View Actions
    func setupAction(){
        self.loginLbl.addAction(for: .tap) {
            let vc = LoginVC.initWithStory()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        self.submitBtn.addAction(for: .tap) {
            self.validation()
        }
        self.checkImg.addAction(for: .tap) {
            if self.checkImg.image == UIImage(named: "unchecked"){
                self.checkImg.image = UIImage(named: "checked")
            }else{
                self.checkImg.image = UIImage(named: "unchecked")
            }
        }
        self.acceptTermsCond.addAction(for: .tap) {
            TermsConditionView.getView.initView(view: self.view)
        }
        
        self.ccTXF.addAction(for: .tap) {
            if let nav = self.navigationController {
                self.ccTXF.showCountriesList(from: nav)
            }
        }
        self.facebookImg.addAction(for: .tap) {
            self.gettingMobileNumberAlert(logintype: "facebook")
        }
        
        self.googleImg.addAction(for: .tap) {
            self.gettingMobileNumberAlert(logintype: "google")
        }
    }
    
    func gettingMobileNumberAlert(logintype : String){
        let alertController = UIAlertController(title: self.Localize.stringForKey(key: "add_phone"), message: self.Localize.stringForKey(key: "phone_signup"), preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = self.Localize.stringForKey(key: "enter_phone")
            textField.keyboardType = .numberPad
            
        }
        let saveAction = UIAlertAction(title: self.Localize.stringForKey(key: "save"), style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            self.mobileNum = firstTextField.text ?? ""
            if !self.mobileNum.isEmpty && !(self.mobileNum.count<10){
                if logintype == "google"{
                    self.googleLogin()
                }else{
                    self.facebookLogin()
                }
            }else{
                showToast(msg: self.Localize.stringForKey(key: "err_mobile"))
            }
        })
        let cancelAction = UIAlertAction(title: self.Localize.stringForKey(key: "cancel"), style: .default, handler: { (action : UIAlertAction!) -> Void in })
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func googleLogin(){
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
//        if GIDSignIn.sharedInstance().hasAuthInKeychain() == true{
//            GIDSignIn.sharedInstance().signInSilently()
//        }
//        else{
            GIDSignIn.sharedInstance().signIn()
//        }
    }
    
    func facebookLogin(){
        let login = LoginManager()
        login.logIn(permissions: ["email"], from: self, handler:  { (FBSDKLoginManagerRequestTokenHandler, Error) in
            if Error != nil {
                print("Login via Facebook Error: \(Error)")
            } else {
                let access  =  FBSDKLoginManagerRequestTokenHandler
                if ( access?.isCancelled == true)
                {
                    return
                }
                let accessToken = AccessToken.current
                if ((accessToken?.userID.count)! > 0)
                {
                    let req = GraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: accessToken?.tokenString, version: nil, httpMethod: HTTPMethod(rawValue: "GET"))
                    req.start(completionHandler: { (connection, result, error) in
                        let res = result as! NSDictionary
                        let lang : String = UserDefaults.standard.value(forKey: UserDefaultsKey.language) as? String ?? ""
                        let fcmid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.fcmtoken) as? String ?? ""
                        
                        // api call
//                        self.signupApi(fname: res["name"]! as! String, lname: "", email: res["email"]! as! String, phone: self.mobileNum, cnty: "", cntyname: "", lang: lang, cur: Constant.priceTag, phcode: "", password: "", referal: "", scId: "", fcmId: fcmid, loginId: res["id"]! as! String , loginType: "facebook")
                    })
                }
            }
        })
    }
    
    //validation
    func validation(){
        let fname : String = self.fireNameTXF.text ?? ""
        let lname : String = self.lastNameTXT.text ?? ""
        let email : String = self.emailAddressTXF.text ?? ""
        let password : String = self.passwordTFX.text ?? ""
        let mobile : String = self.mobileNumTXF.text ?? ""
        let referral : String = self.referralTXF.text ?? ""
        let address : String = self.addressTXF.text ?? ""
        let lang : String = UserDefaults.standard.value(forKey: UserDefaultsKey.language) as? String ?? ""
        let fcmid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.fcmtoken) as? String ?? ""
        
        //firest name
        if !fname.isEmpty{
            setTextfieldApperance(textfild: self.fireNameTXF, title: Localize.stringForKey(key: "first_name"), color: UIColor(named: "AppColor")!, isError: false)
            
            if !(fname.count < 3){
                setTextfieldApperance(textfild: self.fireNameTXF, title: Localize.stringForKey(key: "first_name"), color: UIColor(named: "AppColor")!, isError: false)
                lastNameTXT.becomeFirstResponder()
                // last name
                if !lname.isEmpty{
                    setTextfieldApperance(textfild: self.lastNameTXT, title: Localize.stringForKey(key: "last_name"), color: UIColor(named: "AppColor")!, isError: false)
                    
                    if !(lname.count < 3){
                        setTextfieldApperance(textfild: self.lastNameTXT, title: Localize.stringForKey(key: "last_name"), color: UIColor(named: "AppColor")!, isError: false)
                        emailAddressTXF.becomeFirstResponder()
                        
                        //email
                        if !email.isEmpty{
                            setTextfieldApperance(textfild: self.emailAddressTXF, title: Localize.stringForKey(key: "email_address"), color: UIColor(named: "AppColor")!, isError: false)
                            if email.isValidEmail(){
                                setTextfieldApperance(textfild: self.emailAddressTXF, title: Localize.stringForKey(key: "email_address"), color: UIColor(named: "AppColor")!, isError: false)
                                passwordTFX.becomeFirstResponder()
                                
                                //password
                                if !password.isEmpty{
                                    setTextfieldApperance(textfild: self.passwordTFX, title: Localize.stringForKey(key: "password"), color: UIColor(named: "AppColor")!, isError: false)
                                    
                                    if !(password.count < 6){
                                        setTextfieldApperance(textfild: self.passwordTFX, title: Localize.stringForKey(key: "password"), color: UIColor(named: "AppColor")!, isError: false)
                                        mobileNumTXF.becomeFirstResponder()
                                        
                                        //mobile num
                                        if !mobile.isEmpty{
                                            setTextfieldApperance(textfild: self.mobileNumTXF, title: Localize.stringForKey(key: "mobile_num"), color: UIColor(named: "AppColor")!, isError: false)
                                            
                                            if !(mobile.count < 10){
                                                setTextfieldApperance(textfild: self.mobileNumTXF, title: Localize.stringForKey(key: "mobile_num"), color: UIColor(named: "AppColor")!, isError: false)
                                                addressTXF.becomeFirstResponder()
                                                
                                                
                                                if !address.isEmpty{
                                                    setTextfieldApperance(textfild: self.addressTXF, title: Localize.stringForKey(key: "address"), color: UIColor(named: "AppColor")!, isError: false)
                                                    self.view.endEditing(true)
                                                
                                                //                                                referralTXF.becomeFirstResponder()
                                                
                                                //country code
                                                if !countryCode.isEmpty{
                                                    
                                                    if self.checkImg.image == UIImage(named: "unchecked"){
                                                        view.endEditing(true)
                                                        showToast(msg: Localize.stringForKey(key: "err_agree"))
                                                        
                                                    }else{
                                                        // api call
                                                        self.signupApi(fname: fname, lname: lname, email: email, phone: mobile, cnty: countryData?.code ?? "", cntyname: countryData?.name ?? "", lang: lang, cur: Constant.priceTag, phcode: countryCode, password: password, referal: referral, scId: "", fcmId: fcmid, loginId: "", loginType: "normal")
                                                    }
                                                    
                                                    
                                                }else{
                                                    showToast(msg: "Please select any Phone code")
                                                }
                                                }else{
                                                    setTextfieldApperance(textfild: self.addressTXF, title: Localize.stringForKey(key: "ent_address"), color: UIColor.red, isError: true)
                                                }
                                                
                                            }else{
                                                setTextfieldApperance(textfild: self.mobileNumTXF, title: Localize.stringForKey(key: "err_mobile"), color: UIColor.red, isError: true)
                                            }
                                        }else{
                                            setTextfieldApperance(textfild: self.mobileNumTXF, title: Localize.stringForKey(key: "ent_mobile_num"), color: UIColor.red, isError: true)
                                        }
                                        
                                    }else{
                                        setTextfieldApperance(textfild: self.passwordTFX, title: Localize.stringForKey(key: "err_pass_count"), color: UIColor.red, isError: true)
                                    }
                                }else{
                                    setTextfieldApperance(textfild: self.passwordTFX, title: Localize.stringForKey(key: "ent_password"), color: UIColor.red, isError: true)
                                }
                                
                                
                                
                            }else{
                                setTextfieldApperance(textfild: self.emailAddressTXF, title: Localize.stringForKey(key: "ent_email_address"), color: UIColor.red, isError: true)
                            }
                        }else{
                            setTextfieldApperance(textfild: self.emailAddressTXF, title: Localize.stringForKey(key: "ent_email_address"), color: UIColor.red, isError: true)
                        }
                        
                    }else{
                        setTextfieldApperance(textfild: self.lastNameTXT, title: Localize.stringForKey(key: "err_count"), color: UIColor.red, isError: true)
                    }
                }else{
                    setTextfieldApperance(textfild: self.lastNameTXT, title: Localize.stringForKey(key: "ent_last_name"), color: UIColor.red, isError: true)
                }
            }else{
                setTextfieldApperance(textfild: self.fireNameTXF, title: Localize.stringForKey(key: "err_count"), color: UIColor.red, isError: true)
            }
        }else{
            setTextfieldApperance(textfild: self.fireNameTXF, title: Localize.stringForKey(key: "ent_first_name"), color: UIColor.red, isError: true)
        }
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: .whitespaces)
        if textField.text!.isEmpty {
            if textField == fireNameTXF{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_first_name"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            }
            else if textField == lastNameTXT{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_last_name"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            }
            else if textField == emailAddressTXF{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_email_address"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            }
            else if textField == passwordTFX{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_password"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            } else if textField == mobileNumTXF{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_mobile_num"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
            }
//            setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "err_valid_data"), color: UIColor.red, isError: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.mobileNumTXF{
//            if textField.text?.count == 0{
//                textField.text = "0"
//            }
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func setTextfieldApperance(textfild : SkyFloatingLabelTextField , title : String , color : UIColor,isError : Bool){
        if isError == true{
            textfild.title = title
            textfild.placeholder = title
            textfild.placeholderColor = color
        }else{
            textfild.title = title
            textfild.placeholderColor = color
        }
        textfild.titleColor = color
        textfild.lineColor = color
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.text!.isEmpty{
            if textField == fireNameTXF{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_first_name"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            }
            else if textField == lastNameTXT{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_last_name"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            }
            else if textField == emailAddressTXF{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_email_address"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            }
            else if textField == passwordTFX{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_password"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
                
            } else if textField == mobileNumTXF{
                setTextfieldApperance(textfild: textField as! SkyFloatingLabelTextField, title: Localize.stringForKey(key: "ent_mobile_num"), color: UIColor.red, isError: true)
                textField.becomeFirstResponder()
                
            }
            
        }else{
            if textField == fireNameTXF{
                setTextfieldApperance(textfild: self.fireNameTXF, title: Localize.stringForKey(key: "first_name"), color: UIColor(named: "AppColor")!, isError: false)
                
                lastNameTXT.becomeFirstResponder()
            }
            else if textField == lastNameTXT{
                setTextfieldApperance(textfild: self.lastNameTXT, title: Localize.stringForKey(key: "last_name"), color: UIColor(named: "AppColor")!, isError: false)
                
                emailAddressTXF.becomeFirstResponder()
            }
            else if textField == emailAddressTXF{
                setTextfieldApperance(textfild: self.emailAddressTXF, title: Localize.stringForKey(key: "email_address"), color: UIColor(named: "AppColor")!, isError: false)
                
                passwordTFX.becomeFirstResponder()
            }
            else if textField == passwordTFX{
                setTextfieldApperance(textfild: self.passwordTFX, title: Localize.stringForKey(key: "password"), color: UIColor(named: "AppColor")!, isError: false)
                
                mobileNumTXF.becomeFirstResponder()
            } else if textField == mobileNumTXF{
                setTextfieldApperance(textfild: self.mobileNumTXF, title: Localize.stringForKey(key: "mobile_num"), color: UIColor(named: "AppColor")!, isError: false)
                self.view.endEditing(true)
                //                referralTXF.becomeFirstResponder()
            }
            else if textField == referralTXF{
                //                setTextfieldApperance(textfild: self.referralTXF, title: Localize.stringForKey(key: "referral"), color: UIColor(named: "AppColor")!, isError: false)
                
                self.view.endEditing(true)
                self.validation()
            }
        }
        return true
    }
}


extension SignupVC : CountryPickerViewDelegate, CountryPickerViewDataSource{
    
    func setupCountryPicker(){
        ccTXF.delegate = self
        ccTXF.dataSource = self
        ccTXF.showCountryCodeInView = false
        ccTXF.showPhoneCodeInView = true
        ccTXF.flagImageView.image = UIImage()
        
        
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country){
        self.countryCode = country.phoneCode
        self.countryData = country
    }
}

// google login
extension SignupVC :  GIDSignInDelegate{
    func configureGoogleSignIn()
    {
        GIDSignIn.sharedInstance().clientID = "414505975837-52cfmjkr89r19p74c7uu66fj0v0lqqvq.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
        
    }
    
    //MARK:- Google SignIn
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            GIDSignIn.sharedInstance().currentUser
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let name = user.profile.name
            let email = user.profile.email
            let lang : String = UserDefaults.standard.value(forKey: UserDefaultsKey.language) as? String ?? ""
            let fcmid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.fcmtoken) as? String ?? ""
            // api call
//            self.signupApi(fname: name ?? "", lname: "", email: email ?? "", phone: self.mobileNum, cnty: "", cntyname: "", lang: lang, cur: Constant.priceTag, phcode: "", password: "", referal: "", scId: "", fcmId: fcmid, loginId: userId ?? "", loginType: "google")
        }
        else
        {
            print("\(error.localizedDescription)")
        }
        
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}


// APi Call
extension SignupVC{
    
    func signupApi(fname : String , lname : String , email : String , phone : String , cnty : String, cntyname : String, lang : String, cur : String, phcode : String, password : String, referal : String, scId : String , fcmId : String, loginId : String , loginType : String){
        
        self.loginVM.otpVerificationApi(email: email, phcode: phcode, phone: phone)
        
        self.loginVM.successVerification = {
            self.otpView.initView(view: self.view, pageFrom: "signup", tripRotue: TripStatusModel(), submit: { (otp) in
                if otp == (self.loginVM.otpVerfication?.otp ?? "0").description{
                    self.loginVM.signupApi(fname: fname, lname: lname, email: email, phone: phone, cnty: cnty, cntyname: cntyname, lang: lang, cur: cur, phcode: phcode, password: password, referal: referal, scId: scId, fcmId: fcmId, loginId : loginId , loginType : loginType)
                }else{
                    showToast(msg: "Wrong OTP , Please Enter Corrent One")
                }
            })
        }
        
        self.loginVM.successSignup = {
            
            if let signupResponse = self.loginVM.signupData{
                
                print("signupResponseDriverRes",signupResponse)
                
                UserDefaults.standard.set(signupResponse.token, forKey: UserDefaultsKey.token)
                let jwt = try! decode(jwt: signupResponse.token)
                let data = jwt.body
                
                UserDefaults.standard.set(data["email"] as? String ?? "", forKey: UserDefaultsKey.email)
                UserDefaults.standard.set(data["name"] as? String ?? "", forKey: UserDefaultsKey.name)
                UserDefaults.standard.set(data["id"] as? String ?? "", forKey: UserDefaultsKey.userid)
                UserDefaults.standard.set(signupResponse.token , forKey: UserDefaultsKey.token)
               
                
                self.FBCONNECT.updateDriverDefaultData()
                
                let userDocvc = ManageDocsVC.initWithStory()
                userDocvc.pageFrom = "signup"
                self.navigationController?.pushViewController(userDocvc, animated: true)
            }
            
            //            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
            //            self.appDelegate.window?.rootViewController = MenuRoot
            
        }
        
        self.loginVM.errSignup = {
            self.fireNameTXF.text = ""
            self.lastNameTXT.text = ""
            self.emailAddressTXF.text = ""
            self.passwordTFX.text = ""
            self.mobileNumTXF.text = ""
            self.referralTXF.text = ""
            
            self.fireNameTXF.placeholderColor = UIColor.gray
            self.lastNameTXT.placeholderColor = UIColor.gray
            self.emailAddressTXF.placeholderColor = UIColor.gray
            self.passwordTFX.placeholderColor = UIColor.gray
            self.mobileNumTXF.placeholderColor = UIColor.gray
            self.referralTXF.placeholderColor = UIColor.gray
        }
    }
}

