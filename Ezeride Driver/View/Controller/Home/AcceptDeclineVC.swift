
//  AcceptDeclineVC.swift
//  RebuStar Driver
//
//  Created by Abservetech on 07/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import AVFoundation
import UICircularProgressRing
import MarqueeLabel

class AcceptDeclineVC: UIViewController {

    
    //UI declaraction
    
    @IBOutlet weak var declineBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
  
    @IBOutlet weak var traveTimeLbl: UILabel!
    @IBOutlet weak var dropLbl: MarqueeLabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var totalFareLbl: UILabel!
    @IBOutlet weak var pickupLbl: MarqueeLabel!
    
    @IBOutlet weak var circleProgressView: UICircularProgressRing!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var dropview: UIView!
    
    @IBOutlet weak var mapImg: UIImageView!
    
    //VariableDeclaraction
    
    // var audioPlayer = AVAudioPlayer()
   // var songPlayer = AVAudioPlayer()
    
    //Firebase object
    var FBConnect = FireBaseconnection.instanse
    
    
    var homevm = HomeVM()
    var requestID : String = ""
    var requestType : String = ""
    
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        self.getFBDriverDetails()
        switch UIApplication.shared.applicationState {
        case .background, .inactive:
            self.stopPlayer()
            case .active:
                 self.prepareSongAndSession()
            default:
                break
        }

         
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
          self.stopPlayer()
    }
    
     override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.homevm = HomeVM(dataService: ApiRoot())
        self.setupView()
        self.setupAction()
        self.setupLang()
      
        self.setupData()
        self.observeNotification()
    }
    
   
    
   func prepareSongAndSession() {
        print("cametoplay")
        self.appDelegate.songPlayer.play()
    
    //    do {
   //         songPlayer = try! AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "requestSound", ofType: "mp3")!))
    //        songPlayer.prepareToPlay()
    //        songPlayer.play()
    //        let audiosession = AVAudioSession.sharedInstance()
    //        do {
   //             try! audiosession.setCategory(AVAudioSession.Category.playback)
   //         } catch let sessionError {
   //             print(sessionError)
   //         }
            
 //       } catch let songplayerError {
  //          print(songplayerError)
 //       }
    }
    
    func setupView(){
        self.circleProgressView.isRoundedView = true
        self.mapImg.isRoundedView = true
        var progress : Int = 1
        for value in progress..<100{
            DispatchQueue.main.asyncAfter(deadline: .now()+4) {
                self.circleProgressView.startProgress(to: CGFloat(value), duration: 10){
                    progress = progress+1
                }
            }
        }
    }
    
    func setupAction(){
        
        self.acceptBtn.addAction(for: .tap) {

            self.acceptTrip()
        }
        
        self.declineBtn.addAction(for: .tap) {

            self.declineTrip()
        }
    }
    
    func setupLang(){
        self.declineBtn.setTitle(self.Localize.stringForKey(key: "decline"), for: .normal)
        self.acceptBtn.setTitle(self.Localize.stringForKey(key: "accept"), for: .normal)
    }
    
    
    func setupData(){
        self.getTripData { (tripdata) in
            if let tripdatas : FBDriverDataModel = tripdata {
                self.pickupLbl.text = tripdatas.request.picku_address
                self.dropLbl.text = tripdatas.request.drop_address
                self.traveTimeLbl.text = tripdatas.request.etd
                self.totalFareLbl.text = self.Localize.stringForKey(key: "total_fare") + Constant.priceTag + " \(tripdatas.request.totalFare) "
                self.distanceLbl.text = " \(tripdatas.request.totalKM) \(Constant.distanceUnit)"
                self.requestID = tripdatas.request.request_id
            }
        }
    }
    
    class func initWithStory()->AcceptDeclineVC{
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "AcceptDeclineVC") as! AcceptDeclineVC
        return vc
    }
    
    
    func observeNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(AppBecomeActive), name: .appEnterInForGorund, object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(AppBecomeInActive), name: .appEnterInBackGround, object: nil)
           
     
       }
    
    @objc func AppBecomeActive(_ notification: Notification)
    {
        print("sssssss@@@@@")
         self.prepareSongAndSession()
    }
    
    @objc func AppBecomeInActive(_ notification: Notification)
    {
        self.stopPlayer()
    }

}


extension AcceptDeclineVC{
    func acceptTrip(){
        if !requestID.isEmpty {
            self.homevm.acceptRide(view: self.view, requestId: self.requestID)
        }else{
            showToast(msg: "Sorry!!! count accept your request ,please try again later")
        }
        
        self.homevm.getacceptclouser = {
            if self.requestType == "rideNow"{
                UserDefaults.standard.set( (self.homevm.accept?.tripId ?? 0).description, forKey: UserDefaultsKey.tripId)
                self.setFBTripData()
                 UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
                  self.stopPlayer()
                  NotificationCenter.default.post(name: .appEnterInBackGround, object: nil)
                self.dismiss(animated: true, completion: nil)
            }else{
                self.FBConnect.cancelDeclineEndTrip()
                 UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
                  NotificationCenter.default.post(name: .appEnterInBackGround, object: nil)
                  self.stopPlayer()
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
        self.homevm.eracceptclouser = {
             UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
              NotificationCenter.default.post(name: .appEnterInBackGround, object: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func declineTrip(){
        if !requestID.isEmpty {
            self.homevm.declineRequest(view: self.view, requestId: self.requestID)
        }else{
            showToast(msg: "Sorry!!! count decline your request ,please try again later")
        }
        
        self.homevm.getdeclineclouser = {
            self.declineFBTrip()
             self.stopPlayer()
             UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
              NotificationCenter.default.post(name: .appEnterInBackGround, object: nil)
            self.dismiss(animated: true, completion: nil)
        }
        
        self.homevm.erdeclineclouser = {
            self.stopPlayer()
             UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
              NotificationCenter.default.post(name: .appEnterInBackGround, object: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func stopPlayer(){
        if self.appDelegate.songPlayer.isPlaying{
            self.appDelegate.songPlayer.stop()
        }
     //  do {
         //         songPlayer = try! AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "requestSound", ofType: "mp3")!))
        //          songPlayer.prepareToPlay()
        //          if self.songPlayer.isPlaying{
        //            self.songPlayer.stop()
        //         }
        //          let audiosession = AVAudioSession.sharedInstance()
        //          do {
        //              try! audiosession.setCategory(AVAudioSession.Category.playback)
        //          } catch let sessionError {
        //              print(sessionError)
       //           }
       //
      //        } catch let songplayerError {
      //            print(songplayerError)
      //        }
      
    }
}

//FireBAse Datas
extension AcceptDeclineVC{
    func getFBDriverDetails(){
        self.FBConnect.getdDriversData {[weak self] (driverData) in
            if let self = self {
            if let rideRequest : FBDriverDataModel? = driverData{
                if let trip_id : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? "" {
                    self.requestType = rideRequest?.request.request_type ?? ""
                    if trip_id.isEmpty {
                        if rideRequest?.request.status == "0"{
                            self.stopPlayer()
                             UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
                             NotificationCenter.default.post(name: .appEnterInBackGround, object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
            }
        }
    }
    func setFBTripData(){
        self.FBConnect.CreateFirebaseTripData(status: "1")
    }
    
    func declineFBTrip(){
        self.FBConnect.cancelDeclineEndTrip()
    }
    
    func getTripData(driverdata : @escaping(FBDriverDataModel)->()){
        self.FBConnect.getdDriversData { (driverTripData) in
            driverdata(driverTripData)
        }
    }
}
