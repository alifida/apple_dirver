//
//  InvoiceVC.swift
//  RebuStar Driver
//
//  Created by Abservetech on 15/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import Cosmos
import MarqueeLabel

class InvoiceVC: UIViewController {
    
    @IBOutlet weak var priceTopView: UIView!
    
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var priceTable: UITableView!
    
    @IBOutlet weak var PriceTabeleHeight: NSLayoutConstraint!
    @IBOutlet weak var pageTitleLbl: UILabel!
    @IBOutlet weak var totalFeeLbl: UILabel!
    
    @IBOutlet weak var commentTxt: UITextField!
    @IBOutlet weak var ratingTitle: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    @IBOutlet weak var startRatingView: CosmosView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var dropAddress: MarqueeLabel!
    @IBOutlet weak var currentAddrssLbl: MarqueeLabel!
    @IBOutlet weak var billLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dicountLbl: UILabel!
    @IBOutlet weak var discountPriceLbl: UILabel!
    
    
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var homevm = HomeVM()
    var ratings : String = ""
    var oldLocation = CLLocation()
    var lastUserHeading = Double()
    
    var currentLocation = CLLocation()
    var pickupaddr : String = ""
    var pickupCity : String = ""
    var pickupLoc : CLLocation = CLLocation()
    var dropAddr : String = ""
    var dropCity : String = ""
    var dropLoc : CLLocation = CLLocation()
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.delegate = self
        _locationManager.startMonitoringSignificantLocationChanges()
        _locationManager.distanceFilter = 2
        return _locationManager
        
    }()
    
    
    //Firebase object
    var FBConnect = FireBaseconnection.instanse
    
    var priceTitleArray : [String] = []
    var priceValueArray : [String] = []
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        self.getFBDriverDetails()
        self.homevm = HomeVM(view: self.view, dataService: ApiRoot())
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.setupMapDelegate()
        self.setupView()
        self.setupAction()
        self.setupLang()
        self.setupDelegate()
    }
    
    func setupView(){
        self.priceTopView.layer.cornerRadius = 10
        self.priceTopView.isElevation = 3
        self.addressView.layer.cornerRadius = 10
        self.addressView.isElevation = 3
        self.submitBtn.roundeCornorBorder = 5
        self.priceTable.isElevation = 3
        self.addressView.layer.cornerRadius = 10
        self.priceTable.layer.cornerRadius = 10
        
        commentTxt.placeholder = Localize.stringForKey(key: "enter_comments")
    }
    
    func setupAction(){
      
        self.startRatingView.didFinishTouchingCosmos = { rating in
            self.ratings = rating.description
        }
        
        self.submitBtn.addAction(for: .tap) {
            //            self.dismiss(animated: true, completion: nil)
            
            self.ratings = self.startRatingView.rating.description
            
            var comment : String = self.commentTxt.text ?? ""
            
//            if (!comment.isEmpty && !self.ratings.isEmpty){
                self.riderFeedback(rating: self.ratings, comment: comment)
//            }else{
//                showToast(msg: "Must give Star Rating and comments for you Driver")
//            }
        }
    }
    
    
    func setupLang(){
        self.pageTitleLbl.text = Localize.stringForKey(key: "invoice_title")
        self.totalFeeLbl.text = Localize.stringForKey(key: "total_fare")
        self.billLbl.text = Localize.stringForKey(key: "bill_rate")
        self.dicountLbl.text = Localize.stringForKey(key: "discount_app")
        self.ratingTitle.text = Localize.stringForKey(key: "rate_title")
        self.submitBtn.setTitle(Localize.stringForKey(key: "submit"), for: .normal)
    }
    
    class func initWithStory()->InvoiceVC{
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
        return vc
    }
    
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.PriceTabeleHeight?.constant = self.priceTable.contentSize.height
    }
    
    func setupDate(tripDetails : FBTripDataModel){
        if let details :FBTripDataModel = tripDetails{
            self.currentAddrssLbl.text = details.pickup_address
            self.dropAddress.text = details.Drop_address
            self.totalPriceLbl.text = Constant.priceTag + details.total_fare
            self.discountPriceLbl.text = Constant.priceTag + details.discount
            
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd"
            let date : String = formatter.string(from: Date())
            
            self.dateLbl.text = date
            
            self.priceTitleArray = [self.Localize.stringForKey(key: "distance") ,
                                    self.Localize.stringForKey(key: "time") ,
                                    self.Localize.stringForKey(key: "base_fare") ,
                                    self.Localize.stringForKey(key: "Waiting_Time") ,
                                    self.Localize.stringForKey(key: "waiting_fare") ,
                                    self.Localize.stringForKey(key: "time_fare") ,
//                                    self.Localize.stringForKey(key: "ride_fare") ,
                                    self.Localize.stringForKey(key: "pickup_fee") ,
                                    self.Localize.stringForKey(key: "distance_fare") ,
                                    self.Localize.stringForKey(key: "access_fee") ,
                                    self.Localize.stringForKey(key: "cancelleantion_fee") ,
                                    self.Localize.stringForKey(key: "payment_method")]
            
            
            
            self.priceValueArray = [
                details.distance + " \(Constant.distanceUnit)",
                details.time + " Mins",
                Constant.priceTag + details.basefare,
                details.waitingTime + " Mins",
                decimalDataString(data : details.waiting_fare),
                decimalDataString(data : details.time_fare),
//                decimalDataString(data : details.total_fare),
                decimalDataString(data : details.convance_fare),
                decimalDataString(data : details.distance_fare) ,
                decimalDataString(data : details.tax),
                decimalDataString(data : details.cancel_fare),
                details.pay_type
            ]
            
            
//            if details.isWaiting == "0"{
//                self.priceValueArray.remove(at: 4)
//                self.priceTitleArray.remove(at: 4)
//            }
//
//
//
//
//            if details.trip_type != "flatrate"{
//
//                self.priceValueArray.remove(at: self.priceValueArray.count-5)
//                self.priceTitleArray.remove(at: self.priceValueArray.count-5)
//
//
//                self.priceValueArray.remove(at: self.priceValueArray.count-4)
//                self.priceTitleArray.remove(at: self.priceValueArray.count-4)
//
//
//                self.priceValueArray.remove(at: self.priceValueArray.count-3)
//                self.priceTitleArray.remove(at: self.priceValueArray.count-3)
//            }else{
//                if details.tax == "0"{
//                    self.priceValueArray.remove(at: self.priceValueArray.count-3)
//                    self.priceTitleArray.remove(at: self.priceValueArray.count-3)
//                }
//                if details.isPickup == "0"{
//                    self.priceValueArray.remove(at: self.priceValueArray.count-5)
//                    self.priceTitleArray.remove(at: self.priceValueArray.count-5)
//                }
//            }
            
            
            print("PRICETABLEDATA",self.priceValueArray)
            self.priceTable.reloadData()
        }
    }
}


extension InvoiceVC: UITableViewDataSource,UITableViewDelegate{
    
    func setupDelegate(){
        self.priceTable.register(UINib(nibName: "PriceCell", bundle: nil), forCellReuseIdentifier: "PriceCell")
        self.priceTable.delegate = self
        self.priceTable.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.priceTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceCell", for: indexPath) as! PriceCell
        cell.priceTitlelbl.text = self.priceTitleArray[indexPath.row]
        if self.priceTitleArray.count == self.priceValueArray.count{
            cell.priceLbl.text = self.priceValueArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}


extension InvoiceVC{
    func riderFeedback(rating : String , comment : String){
        let tripid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? ""
        self.homevm.riderFeedBack(view: self.view, tripId: tripid, rating: rating, comments: comment)
        self.homevm.getfeedbackClouser = {
            showToast(msg: self.homevm.feedback?.message ?? "")
            self.FBConnect.cancelDeclineEndTrip()
           
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripId)
            self.updateFBLocation(loc: self.currentLocation)
            
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
                let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
                self.appDelegate.window?.rootViewController = MenuRoot
            })
        }
    }
}
// Map location Function
extension InvoiceVC :  CLLocationManagerDelegate{
    
    // it enable mapdelegate and location button [163 to 185]
    func setupMapDelegate(){
        
        //Enable Location Service in mobile
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
        else
        {
            showToast(msg: "Please enable the location service in settings")
        }
        
        self.locationManager.startUpdatingLocation()
        
    }
    
    
    // MARK : Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .authorizedAlways:
            self.locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
            
        case .notDetermined:
            self.locationManager.requestAlwaysAuthorization()
        case .denied:
            print("User denied access to location.")
        }
    }
    
    // MARK: Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    // MARK: update Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.oldLocation = locations.first!
        //        self.currentLocation = locations.last ?? CLLocation()
        var location: CLLocation = locations.last ?? CLLocation()
        location = CLLocation(coordinate: location.coordinate, altitude: location.distance(from: location), horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: self.lastUserHeading, speed: location.speed, timestamp: Date())
        self.currentLocation = location
        
        print("*****CurrentLocation",self.currentLocation)
        
        
        
    }
}
extension InvoiceVC{
    func getFBDriverDetails(){
        self.FBConnect.getTripData { (tripDetails) in
            self.setupDate(tripDetails: tripDetails)
        }
    }
    
    func updateFBLocation(loc : CLLocation){
        self.FBConnect.updatedefatulLoc(loc: loc)
    }
}
