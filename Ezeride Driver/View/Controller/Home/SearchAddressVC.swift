//
//  SearchAddressVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 04/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import GoogleMaps

enum TappedPlace : String {
    case none = "none"
    case pickup = "pickup"
    case drop = "drop"
}

class SearchAddressVC: UIViewController,UITextFieldDelegate {
    
    //Api Reponse
    var searchAddress : GoogleAddressModel?{
        didSet{
            self.addressTabelView.reloadData()
        }
    }
    
    //UI Declaractions
    
    @IBOutlet weak var backArrowIMG: ImageLoader!
    @IBOutlet weak var pickupCliseImg: ImageLoader!
    @IBOutlet weak var dropCloseImg: ImageLoader!
    
    @IBOutlet weak var pickupTXF: UITextField!
    @IBOutlet weak var searchTXF: UITextField!
    
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var dropView: UIView!
    
    @IBOutlet weak var setupPinLBL: UILabel!
    
    @IBOutlet weak var addressTabelView: UITableView!
    
    
    
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var currentAddress : String = ""
    
    var googleApi = GoogleVM()
    
    var tappedPlace : String = TappedPlace.pickup.rawValue
    
    var tripRouteDelegate : TripRoutes?
    
    var pickupaddr : String = ""
    var pickupCity : String = ""
    var pickupLoc : CLLocation = CLLocation()
    var dropAddr : String = ""
    var dropCity : String = ""
    var dropLoc : CLLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.setupAction()
        self.setupView()
        self.setupLang()
        self.setupDelegate()
        self.setupApiIntialization()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        self.setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupView(){
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.revealViewController().rearViewRevealWidth = 300
        
        // set border for from label
        self.dropView.roundeCornorBorder = 5
        self.pickupView.roundeCornorBorder = 5
        
        self.pickupCliseImg.tintColor = UIColor(named: "AppColor")
        self.dropCloseImg.tintColor = UIColor(named: "AppColor")
        self.backArrowIMG.tintColor = UIColor.white
        
    }
    
    func setupData(){
        self.pickupTXF.text = self.currentAddress
    }
    
    func setupDelegate(){
        self.pickupTXF.delegate = self
        self.searchTXF.delegate = self
        
        self.addressTabelView.delegate = self
        self.addressTabelView.dataSource = self
        self.addressTabelView.register(UINib(nibName: "FavAddressCell", bundle: nil), forCellReuseIdentifier: "FavAddressCell")
        self.addressTabelView.reloadData()
    }
    
    func setupAction(){
        self.backArrowIMG.addAction(for: .tap) {
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.popViewController(animated: true)
        }
        self.pickupCliseImg.addAction(for: .tap) {
            self.pickupTXF.text = ""
            self.currentAddress = ""
        }
        self.dropCloseImg.addAction(for: .tap) {
            self.searchTXF.text = ""
        }
    }
    
    func setupLang(){
        self.pickupTXF.placeholder = Localize.stringForKey(key: "pickupaddr")
        self.searchTXF.placeholder = Localize.stringForKey(key: "search")
        self.setupPinLBL.text = Localize.stringForKey(key: "septupin")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.pickupTXF{
            self.tappedPlace = TappedPlace.pickup.rawValue
            self.getNearByAddr(address: textField.text ?? "")
        }else if textField == self.searchTXF{
            self.tappedPlace = TappedPlace.drop.rawValue
            self.getNearByAddr(address: textField.text ?? "")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    class func initWithStory()->SearchAddressVC{
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
        return vc
    }
}

extension SearchAddressVC :UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.searchAddress?.predictions?.count{
            if count > 0 {
                ShowMsginWindow.instanse.hideNodataView()
                return count
            }else{
                ShowMsginWindow.instanse.nodataView(view: self.view)
            }
        }
        ShowMsginWindow.instanse.nodataView(view: self.view)
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavAddressCell", for: indexPath) as! FavAddressCell
        self.setupTableviewData(cell: cell, index: indexPath.row, address: self.searchAddress!)
        cell.deleteImage.isHidden = true
//        cell.deleteImage.addAction(for: .tap) {
//            self.deleteAddress(delete: cell.deleteImage)
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       
        switch self.tappedPlace {
        case TappedPlace.pickup.rawValue:
            self.pickupTXF.text = self.searchAddress?.predictions?[indexPath.row].descriptionField ?? ""
            self.navigatetoHomeWithAddress()
            break
        case TappedPlace.drop.rawValue:
            self.searchTXF.text = self.searchAddress?.predictions?[indexPath.row].descriptionField ?? ""
           self.navigatetoHomeWithAddress()
            break
        default:
            self.pickupTXF.text = self.searchAddress?.predictions?[indexPath.row].descriptionField ?? ""
        }
        
       
    }
    
    func deleteAddress(delete : UIImageView){
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
            actionSheet.addAction(UIAlertAction(title: Localize.stringForKey(key: "delete"), style: .default, handler: {(action) in
                
            }))
        
            if let popview = actionSheet.popoverPresentationController {
                popview.sourceView = delete
                popview.sourceRect = delete.bounds
            }
            self.present(actionSheet, animated: true, completion: nil)
    }
    
    func setupTableviewData(cell : FavAddressCell , index : Int, address : GoogleAddressModel){
        if let getAddress = address.predictions?[index]
        {
            cell.addressLbl.text = getAddress.descriptionField ?? "YYYY"
            if let title = getAddress.structuredFormatting?.mainText {
                cell.addrTitleLbl.text = getAddress.structuredFormatting?.mainText ?? ""
            }else{
                let address : String = getAddress.descriptionField ?? "" as String
                var addressArray = address.components(separatedBy: ",")
                cell.addrTitleLbl.text = addressArray[0] as? String ?? ""
            }
        }
    }
    
    func navigatetoHomeWithAddress(){
        if !(self.pickupTXF.text?.isEmpty ?? false) && !(self.searchTXF.text?.isEmpty ?? false)
        {
            self.pickupaddr = self.pickupTXF.text ?? ""
            self.dropAddr = self.searchTXF.text ?? ""
            // getting pickuplatlang first the droplatlang then move to homepage
            convertAddressTOLatLang(address: self.pickupaddr, latlang: {(location) in
                self.pickupLoc = location
                convertAddressTOLatLang(address: self.dropAddr, latlang: {(location) in
                    self.dropLoc = location
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.tripRouteDelegate?.getPickupDropLocation(pickAddr: self.pickupaddr, pickupLoc: self.pickupLoc, dropAddr: self.dropAddr, dropLoc: self.dropLoc)
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            })
        }
    }
}


// APi call
extension SearchAddressVC{
    
    func setupApiIntialization(){
        self.googleApi = GoogleVM(view: self.view, dataService: ApiRoot())
    }
    
    func getNearByAddr(address : String){
        self.googleApi.nearByLocation(input: address)
        
        self.googleApi.showAddressClosure = {
            self.searchAddress = self.googleApi.getAddressList
        }
        
        self.googleApi.errorAddressClosure = {
            self.searchAddress = self.googleApi.getAddressError
        }
    }
    
}
