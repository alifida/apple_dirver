//
//  HomeVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 31/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import GoogleMaps
import PINRemoteImage
import MessageUI


/* view hiddenstates
 1. === hide vehicle list and show from label
 2. ==  hide from lable and show vehcile list with map marker for trip route
 3.
 */

/* subview lists
 1. for add vehcile lust subview
 2. for add extimate list subview
 */

protocol TripRoutes {
    func getPickupDropLocation(pickAddr : String,pickupLoc : CLLocation , dropAddr : String , dropLoc : CLLocation)
    func getPinLocation(tag : String , addr : String , addrLoc : CLLocation)
}

extension HomeVC : TripRoutes{
    
    func getPinLocation(tag: String, addr: String, addrLoc: CLLocation) {
        self.mapView.clear()
        if tag == "pickup"{
            self.pickupaddr = addr
            self.pickupLoc = addrLoc
            self.setPolyLineWithMaker(pickupaddr: addr, dropaddr: dropAddr, pickupLoc: addrLoc, dropLoc: dropLoc,tripSttaus : "")
        }else{
            self.dropAddr = addr
            self.dropLoc = addrLoc
            self.setPolyLineWithMaker(pickupaddr: pickupaddr, dropaddr: addr, pickupLoc: pickupLoc, dropLoc: addrLoc,tripSttaus : "")
        }
        
    }
    
    
    func getPickupDropLocation(pickAddr: String, pickupLoc: CLLocation, dropAddr: String, dropLoc: CLLocation) {
        self.mapView.clear()
        self.pickupaddr = pickAddr
        self.pickupLoc = pickupLoc
        self.dropAddr = dropAddr
        self.dropLoc = dropLoc
        self.setPolyLineWithMaker(pickupaddr: pickAddr, dropaddr: dropAddr, pickupLoc: pickupLoc, dropLoc: dropLoc, tripSttaus: "")
        
     
    }
}


class HomeVC: UIViewController ,MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Api response
    
    
    //UI Declaraction
    
    @IBOutlet weak var menuImg: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var onlineImg: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var proofStatusView: UIView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var nvmenuView: UIView!
    
    @IBOutlet weak var proofstatusLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var defaultVehicleName: UILabel!
    @IBOutlet weak var changeVehcileLbl: UILabel!
    
    //Variyable Declaraction
    
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var currentLocation = CLLocation()
    var pickupaddr : String = ""
    var pickupCity : String = ""
    var pickupLoc : CLLocation = CLLocation()
    var dropAddr : String = ""
    var dropCity : String = ""
    var dropLoc : CLLocation = CLLocation()
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.delegate = self
        _locationManager.startMonitoringSignificantLocationChanges()
        _locationManager.distanceFilter = 5
        return _locationManager
        
    }()
    
    // variable for animated pollyline
    var timer: Timer!
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    //Live Tracking variables
    var userBearing = Double()
    var lastUserHeading = Double()
    var previousUserHeading = Double()
    var lastMapBearing = Double()
    var trackingMarker = GMSMarker()
    let pickupMarker = GMSMarker()
    let dropMarker = GMSMarker()
    
    var oldLocation = CLLocation()
    var flagMapFirstTime = true
    var rotationAngle:Double! = 0.0
    var zoomFloat = 15.0
    var i: UInt = 0
    var travelDistance : String = ""
    var travelDuraction : String = ""
    
    var focusZoom : Float = 12
    var selectedVCIndex : Int = -1
    var currentAddress : String = ""
    var subviewCount : Int = -1
    var hiddenShowViews : Int = 1
    
    var startlocations : CLLocation = CLLocation()
    var actualtravelDistance : Float = 0.0
    var tripEndData : String = ""
    var homevm = HomeVM()
    var googleVM = GoogleVM()
    var profile = ProfileVM()
    
    //displace Views
    let rideStatusView = RideStartEndView.getView
    let rideDetailView = RideDetailView.getView
    let otpView = OTPView.getView
    let vehicleView = VehicleListView.getView
    
    //Models
    var tripRouteStatus : TripStatusModel = TripStatusModel()
    
    //Firebase object
    var FBConnect = FireBaseconnection.instanse
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        print("ProfileConstant",Constant.profileData)
        print("currentTaxi", Constant.currentTaxi._id)
        self.homevm = HomeVM(dataService: ApiRoot())
        self.mapView.clear()
        self.userView.alpha = 0.1
        
        self.googleVM = GoogleVM(view: self.view, dataService: ApiRoot())
        profile = ProfileVM(dataService: ApiRoot())
        
        self.setupAction()
        self.setupView()
        self.setupLang()
       self.setupDelegate()
        self.setupMapDelegate()
        self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
        self.observeNotification()
        
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        profile = ProfileVM(dataService: ApiRoot())
        self.userView.alpha = 0.1
        self.getFBDriverDetails()
        self.getFBTripData()
        self.getProfileData()
        self.getFBCancelLationDetails()
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
        if timer != nil{
            self.timer.invalidate()
        }
    }
    
    func setupView(){
        //self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.revealViewController().rearViewRevealWidth = 300
        
        self.nvmenuView.isRoundedView = true
        self.nvmenuView.layer.masksToBounds = true
        self.nvmenuView.isElevation = 3
        
        self.userView.leftRightRoundCorners(radius: 20)
        self.userView.isElevation = 3
        self.userImage.isRoundedView = true
        
        self.proofStatusView.isElevation = 3
        self.proofStatusView.layer.cornerRadius = 10
        
        changeVehcileLbl.text = Localize.stringForKey(key: "change")
        
    }
    
    
    
    func mapPadding(addBottom : CGFloat , reduceBottom : CGFloat){
        
        /* add reduce means we want to set the buttom based on view height and based on screen height
            if we use screen height means we want to add height
            if we use view hight (subviews) we want to reduce the height
         */
        
        switch self.subviewCount {
        case 1:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 150+addBottom-reduceBottom, right: 0)
           
         case 2:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: PaymentDetailView.getView.frame.height+addBottom-reduceBottom * 1.5, right: 0)
        case 3:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: TripStatusView.getView.frame.height+addBottom-reduceBottom+20, right: 0)
        default:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }

    
    func setupDelegate(){
    }
    
    func setupAction(){
        self.menuImg.addAction(for: .tap) {
            if self.revealViewController() != nil {
                self.revealViewController().revealToggle(animated: true)
            }
        }
        
        self.startView.addAction(for: .tap) {
            if self.onlineImg.image == UIImage(named: "red"){
//                let credits : Double = Double(Constant.credits) as? Double ?? 0.0
//                let lowblnc : Double = Double(Constant.lowbalnce) as? Double ?? 0.0
//                if credits <= lowblnc{
//                    let alert = UIAlertController(title: self.Localize.stringForKey(key: "low_blc"), message: Constant.miniBlncAlrt, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: self.Localize.stringForKey(key: "ok"), style: UIAlertAction.Style.default, handler: nil))
//
//                    self.present(alert, animated: true, completion: nil)
//                }else{
                 self.setOnlinOffLine(online: "1")
//                }
            }else if self.onlineImg.image == UIImage(named: "green"){
                 self.setOnlinOffLine(online: "0")
            }
        }
        self.changeVehcileLbl.addAction(for: .tap) {
            self.vehicleView.initView(view: self.view, addvicile: { (addVehile) in
               self.vehicleView.deInitView()
                let vc = AddVechileVC.initWithStory()
                self.navigationController?.pushViewController(vc, animated: true)
            }, manageVehicle: { (managaeVehicle) in
                self.vehicleView.deInitView()
                let vc = ManageVehicleVC.initWithStory()
                self.navigationController?.pushViewController(vc, animated: true)
                
            },closeView :{  (closeview) in
                self.setupData()
                self.vehicleView.deInitView()
            
            })
        }
    }
    
    func setupLang(){
        
    }
    
    func setupData(){
        if let profile : ProfileModel = Constant.profileData{
            self.userView.alpha = 1
            self.userNameLbl.text = profile.fname + " " + profile.lname
            var urls : String = ServiceApi.Base_Image_URL+profile.profile ?? String()
            self.userImage?.pin_setImage(from: URL(string: urls))
            self.userImage.clipsToBounds = true
            self.userImage.layer.cornerRadius = self.userImage.frame.width / 2
            
            
            if profile.online{
                self.updateLocation(Status: "1")
                 self.setOnlinOffLine(online: "1")
                self.onlineImg.image = UIImage(named: "green")
                self.statusLbl.text = self.Localize.stringForKey(key: "available")
                self.updateFBLocation(loc: self.currentLocation)
                
            }else{
                self.onlineImg.image = UIImage(named: "red")
                self.statusLbl.text = self.Localize.stringForKey(key: "unavailable")
                 self.setOnlinOffLine(online: "0")
                self.updateLocation(Status: "0")
            }
            if let vehicle : CurrentActiveTaxi = Constant.currentTaxi{
                if vehicle.vehicletype.isEmpty{
                    self.defaultVehicleName.isHidden = true
                    self.changeVehcileLbl.isHidden = true
                }else{
                    self.defaultVehicleName.isHidden = false
                    self.changeVehcileLbl.isHidden = false
                }
                self.defaultVehicleName.text = vehicle.vehicletype + "( \(vehicle.makename) )"
                UserDefaults.standard.set((vehicle.vehicletype ?? "").lowercased(), forKey: UserDefaultsKey.defaultVehicle)
            }
        }
    }
    
    
    func endRider(){
        self.mapView.clear()
        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripId)
        self.rideStatusView.deInitView()
        self.rideDetailView.deInitView()
        self.otpView.deInitView()
        self.vehicleView.deInitView()
        self.FBConnect.cancelDeclineEndTrip()
    }
    
    class func initWithStory()->HomeVC {
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        return vc
    }
    
}


//Listen Notification centers
extension HomeVC {
    func observeNotification(){
         NotificationCenter.default.addObserver(self, selector: #selector(closeEstimateView(_:)), name: .closeEstimateFare, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addedEstimateFare(_:)), name: .addedEstimateFare, object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(requestData(_:)), name: .requestedView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(changedCurrentTaxi(_:)), name: .changedCurrentTaxi, object: nil)
    }
    
    @objc func requestData(_ notification : Notification){
        if let data = notification.userInfo as? [String: String]
        {
            for (name, score) in data
            {
                if name == "requestHeight"{
                    let height : CGFloat = CGFloat(Float(score) ?? 0.0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.subviewCount = 3
                        self.mapPadding(addBottom: 0.0, reduceBottom: height)
                    }
                }
            }
        }
    }
    
    @objc func addedEstimateFare(_ notification : Notification){
        if let data = notification.userInfo as? [String: String]
        {
            for (name, score) in data
            {
                if name == "height"{
                    let height : CGFloat = CGFloat(Float(score) ?? 0.0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.mapPadding(addBottom: 0.0, reduceBottom: height)
                    }
                }
            }
        }
    }
    
    @objc func closeEstimateView(_ notification: Notification)
    {
        self.subviewCount = 1
        self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
    }
    
    @objc func changedCurrentTaxi(_ notification : Notification){
        self.setupData()
    }
}


// Map location Function
extension HomeVC : GMSMapViewDelegate, CLLocationManagerDelegate{
   
    // it enable mapdelegate and location button [163 to 185]
    func setupMapDelegate(){
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.settings.compassButton = true
        
        //Enable Location Service in mobile
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
        else
        {
            showToast(msg: "Please enable the location service in settings")
        }
        
        self.locationManager.startUpdatingLocation()
        
    }
    
    
    // MARK : Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .authorizedAlways:
            self.locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
            
        case .notDetermined:
            self.locationManager.requestAlwaysAuthorization()
        case .denied:
            print("User denied access to location.")
        }
    }
    
    // MARK: Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    // MARK: update Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
        self.oldLocation = locations.first!
//        self.currentLocation = locations.last ?? CLLocation()
       var location: CLLocation = locations.last ?? CLLocation()
        location = CLLocation(coordinate: location.coordinate, altitude: location.distance(from: location), horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: self.lastUserHeading, speed: location.speed, timestamp: Date())
        self.currentLocation = location
        
        print("*****CurrentLocation",self.currentLocation)
        
        // focus to current  location
        let tripid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? ""
        if tripid.isEmpty{
            self.mapView.camera = GMSCameraPosition(target: self.currentLocation.coordinate, zoom: self.focusZoom, bearing: 0, viewingAngle: 0)
            
        }
        
        // getting current Address
        convertLatLangTOAddress(coordinates: self.currentLocation, address: { (address) -> Void in
            self.currentAddress = address
            print("*****Getting_Address" , self.currentAddress)
        })
        
        //update location in Firebase based on online status
        if Constant.profileData.online{
            self.updateFBLocation(loc: self.currentLocation)
            self.updateLocation(Status: "1")
        }else{
            self.updateLocation(Status: "0")
//            self.locationManager.stopUpdatingLocation()
        }
        
    }
    
    //get the vehicle turning angle
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
               if (self.previousUserHeading - self.lastUserHeading) > 10.0{
                    lastUserHeading = newHeading.trueHeading as Double
                }else{
                    lastUserHeading = self.previousUserHeading
                }
                
                self.rotationAngle = (self.lastUserHeading - self.lastMapBearing)
                
                var location: CLLocation = self.currentLocation
                location = CLLocation(coordinate: location.coordinate, altitude: location.distance(from: location), horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: self.lastUserHeading, speed: location.speed, timestamp: Date())
                
                self.currentLocation = location
//                self.loadTrackingMarker(fromLocation: self.oldLocation, toLocation: self.currentLocation, bearing: self.lastUserHeading)
        
            self.previousUserHeading = newHeading.trueHeading as Double
        }
    
    
    func setDriverLocationMarker(driverLoc : CLLocation){
        let driverMarker = GMSMarker()
        driverMarker.icon = UIImage(named: "car_maker.png")
        driverMarker.map = self.mapView
        driverMarker.isFlat = true
        driverMarker.position = CLLocationCoordinate2D(latitude: driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude)
        driverMarker.rotation = driverLoc.course
        
        if(!isMarkerWithinScreen(marker: driverMarker)){
            let fancy = GMSCameraPosition.camera(withLatitude:  driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude, zoom: 10, bearing: driverLoc.course, viewingAngle: 0)
            self.mapView.animate(to: fancy)
        }
    }
    
    func getTravelDistance(pickupaddr: String, dropaddr: String,pickupLoc : CLLocation , dropLoc : CLLocation, tripSttaus : String,tripstatus : FBTripDataModel){
        let pickuplat : String = UserDefaults.standard.value(forKey: UserDefaultsKey.pickuplat) as? String ?? "0.0"
        let pickuplang : String = UserDefaults.standard.value(forKey: UserDefaultsKey.pickuplang) as? String ?? "0.0"
        let pickupaddr = UserDefaults.standard.value(forKey: UserDefaultsKey.pickupaddrs)
        let origin = "\(pickuplat),\(pickuplang)"
        let destination = "\(dropLoc.coordinate.latitude),\(dropLoc.coordinate.longitude)"
        
        self.googleVM.getPolylineTimeTravel(origin: origin, destination: destination)
        
        self.googleVM.directionClosure = {
            let distant : String = self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 \(Constant.distanceUnit)"
                let distarray = distant.split(separator: " ")
            if distarray.count>0{
                if distarray[1] == "m"{
                    let kmdis : String = ((Double(String(distarray[0]) as String ?? "0.0") ?? 0.0) / 1000.0).description
                    self.travelDistance = kmdis + " \(Constant.distanceUnit)"
                }else{
                    self.travelDistance = self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 \(Constant.distanceUnit)"
                    
                }
            }
                self.travelDuraction = self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text ?? "0 mins"
            self.updateTripStatus(location: self.currentLocation, address: self.currentAddress, status: "4", tripstatus: tripstatus, isButtonclick: false)
          
        }
        
        self.googleVM.errDirectionClouser = {
            self.updateTripStatus(location: self.currentLocation, address: self.currentAddress, status: "4", tripstatus: tripstatus, isButtonclick: false)
        }
    }
        
        
    func setPolyLineWithMaker(pickupaddr: String, dropaddr: String,pickupLoc : CLLocation , dropLoc : CLLocation, tripSttaus : String){
        let origin = "\(pickupLoc.coordinate.latitude),\(pickupLoc.coordinate.longitude)"
        let destination = "\(dropLoc.coordinate.latitude),\(dropLoc.coordinate.longitude)"
        
        self.googleVM.getPolylineTimeTravel(origin: origin, destination: destination)
        
        self.googleVM.directionClosure = {
            
            guard let drirection = self.googleVM.getDirection else {return}
           
            self.path = GMSPath(fromEncodedPath:  self.googleVM.getDirection?.routes?[0].overviewPolyline?.points ?? "")!
            self.polyline.path = self.path
            self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            self.polyline.strokeWidth = 3.0
            self.polyline.title =  "\(String(describing: self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text))\n\(self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text)"
            self.polyline.map = self.mapView

//            self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
            
            var bounds = GMSCoordinateBounds()
            for index in 1...self.path.count() {
                bounds = bounds.includingCoordinate(self.path.coordinate(at: index))
            }
            
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds,withPadding: 100))
            let pickupRoadPoin : CLLocation = CLLocation(latitude: Double(self.googleVM.getDirection?.routes?[0].legs?[0].startLocation?.lat ?? 0.0), longitude:  Double(self.googleVM.getDirection?.routes?[0].legs?[0].startLocation?.lng ?? 0.0))
            let dropRoadPoin : CLLocation = CLLocation(latitude: Double(self.googleVM.getDirection?.routes?[0].legs?[0].endLocation?.lat ?? 0.0), longitude:  Double(self.googleVM.getDirection?.routes?[0].legs?[0].endLocation?.lng ?? 0.0))
            
            self.setMaker(pickupaddr: pickupaddr, dropaddr: dropaddr,pickupLoc : pickupRoadPoin ,dropLoc : dropRoadPoin , time :self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text ?? "0 mins" , distance :self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 km" )
            
//            if tripSttaus == "3"{
//                self.travelDistance = self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 km"
//                self.travelDuraction = self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text ?? "0 mins"
//            }
            
//            self.setInfoWindow(pickupaddr: pickupaddr, dropaddr: dropaddr,pickupLoc : pickupLoc ,dropLoc : dropLoc , time : self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 km", distance : self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text ?? "o mins" )
        }
        
        
        
        self.googleVM.errDirectionClouser = {
            self.hiddenShowViews = 1
            self.subviewCount = 0
            self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
        }
    }
    
    @objc func animatePolylinePath() {
        
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    func setMaker(pickupaddr : String , dropaddr : String,pickupLoc : CLLocation , dropLoc : CLLocation ,time : String , distance : String ){
        pickupMarker.icon = UIImage(named: "pickup_marker.png")
        pickupMarker.map = self.mapView
        pickupMarker.isFlat = true
        pickupMarker.position = CLLocationCoordinate2D(latitude: pickupLoc.coordinate.latitude, longitude: pickupLoc.coordinate.longitude)
        dropMarker.icon = UIImage(named: "drop_marker.png")
        dropMarker.map = self.mapView
        dropMarker.isFlat = true
        dropMarker.position = CLLocationCoordinate2D(latitude: dropLoc.coordinate.latitude, longitude: dropLoc.coordinate.longitude)
         }
    
 
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        let location = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
      
    }
    
    func loadTrackingMarker(fromLocation : CLLocation , toLocation : CLLocation , bearing : Double){
        
                self.trackingMarker.icon = UIImage(named: "car_maker")
                self.trackingMarker.map = self.mapView
                self.trackingMarker.isFlat = true
                self.trackingMarker.position = CLLocationCoordinate2D(latitude: toLocation.coordinate.latitude, longitude:
                    toLocation.coordinate.longitude)
                trackingMarker.rotation =  bearing
//                if(!isMarkerWithinScreen(marker: trackingMarker)){
//                    let fancy = GMSCameraPosition.camera(withLatitude: toLocation.coordinate.latitude, longitude: toLocation.coordinate.longitude, zoom: 10, bearing: self.lastMapBearing, viewingAngle: 0)
//                    self.mapView.animate(to: fancy)
//                }
        
    }
    
    func isMarkerWithinScreen(marker: GMSMarker) -> Bool {
        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        return bounds.contains(marker.position)
    }
    
    //MARK: angle Heading
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        self.lastMapBearing = position.bearing
        self.rotationAngle = -(self.lastUserHeading - self.lastMapBearing)
        
        if(self.flagMapFirstTime){
            zoomFloat = 14
            self.flagMapFirstTime = false
        }else{
            zoomFloat = Double(mapView.camera.zoom)
        }
    }
}

//APi call
extension HomeVC{
    func setOnlinOffLine(online: String){
        self.homevm.changeOnlineStatus(view: self.view, status: online)
        self.homevm.getOnlineclouser = {
            if self.homevm.online?.message == "Online" {
                self.onlineImg.image = UIImage(named: "green")
                self.statusLbl.text = self.Localize.stringForKey(key: "available")
                Constant.profileData.online = true
                self.onlineOfflineStatus(Status: "1")
                self.updateLocation(Status: "1")
                
              
            }else {
                self.onlineImg.image = UIImage(named: "red")
                self.statusLbl.text = self.Localize.stringForKey(key: "unavailable")
                Constant.profileData.online = false
                self.onlineOfflineStatus(Status: "0")
                self.updateLocation(Status: "0")
            }
        }
    }
    
    func getProfileData(){
        self.profile.getProfile()
        self.profile.successprofile = {
            self.setupData()
        }
    }
    
    func updateLocation(Status : String){
        self.homevm.updateLocation( loc: self.currentLocation, status: Status)
//        self.setDriverLocationMarker(driverLoc: self.currentLocation)
        self.loadTrackingMarker(fromLocation: self.oldLocation, toLocation: self.currentLocation, bearing: self.lastUserHeading)
        if Status == "1"{
        self.updateFBLocation(loc: self.currentLocation)
        }
    }
    
    func updateTripStatus(location : CLLocation , address : String , status : String,tripstatus : FBTripDataModel,isButtonclick : Bool){
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let time : String = formatter.string(from: Date())
        
        let tripid = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? ""
        let lat : String = (location.coordinate.latitude ?? 0.0).description
        let lang : String = (location.coordinate.longitude ?? 0.0).description
        
        if status == "3"{
            
            self.homevm.tripCurrentStatus(view: self.view, allowanceDistance: "", pickupLat: lat, pickupLng: lang, startTime: time, fromAddress: address, endAddress: "", endTime: "", waitingTime: "", waitingSecond: "", additionalFee: "", dropLng: "", dropLat: "", duration: "", tripId: tripid, status: status, distance: "")
            
        }else if status == "4"{
             let duraction = self.travelDuraction.split(separator: " ")
            var strduraction : String = "0.00"
            var strdistance : String = "0"
            
            if duraction.count > 0{
             strduraction = duraction[0].description
            }
            let distance = self.travelDistance.split(separator: " ")
            if distance.count > 0{
                strdistance = distance[0].description
            }
            self.homevm.tripCurrentStatus(view: self.view, allowanceDistance: "", pickupLat: "", pickupLng: "", startTime: "", fromAddress: "", endAddress: address, endTime: time, waitingTime: "", waitingSecond: "", additionalFee: "", dropLng: lang, dropLat: lat, duration: strduraction, tripId: tripid, status: status, distance: String(format: "%.3f", actualtravelDistance))
        }else{
            self.homevm.tripCurrentStatus(view: self.view, allowanceDistance: "", pickupLat: "", pickupLng: "", startTime: "", fromAddress: "", endAddress: "", endTime: "", waitingTime: "", waitingSecond: "", additionalFee: "", dropLng: "", dropLat: "", duration: "", tripId: tripid, status: status, distance: "")
        }
        
     
        
        self.homevm.getTripRouteClouser = {
            self.tripRouteStatus = self.homevm.tripRoute ?? TripStatusModel()
            self.RouteRiderOTPViews(tripstatus: tripstatus)
            self.listenDriverLocationForPolyline(tripstatus: status, rideDetails: self.tripRouteStatus)
            if status == "2" || status == "3"{
                if status == "3"{
                    if isButtonclick{
                    UserDefaults.standard.set(lat.description, forKey: UserDefaultsKey.pickuplat)
                    UserDefaults.standard.set(lang.description, forKey: UserDefaultsKey.pickuplang)
                    UserDefaults.standard.set(address, forKey: UserDefaultsKey.pickupaddrs)
                    UserDefaults.standard.set("true", forKey: UserDefaultsKey.isstarted)
                                   }
                    self.updateFBLocation(loc: location)
                }
                self.updateTripSttaus(Status: status)
            }else if status == "4"{
                self.rideStatusView.deInitView()
                self.rideDetailView.deInitView()
                let vc = InvoiceVC.initWithStory()
                 vc.modalPresentationStyle = .fullScreen
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func listenDriverLocationForPolyline(tripstatus : String,rideDetails : TripStatusModel){
        let driverLoc : CLLocation = CLLocation(latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude)
         var startLoc : CLLocation = CLLocation()
        var endLoc : CLLocation = CLLocation()
        if rideDetails.routeData.startcoords.count > 0{
              startLoc  = CLLocation(latitude: CLLocationDegrees(rideDetails.routeData.startcoords[1]), longitude: CLLocationDegrees(rideDetails.routeData.startcoords[0]))
        }
        if rideDetails.routeData.endcoords.count > 0{
            endLoc = CLLocation(latitude: CLLocationDegrees(rideDetails.routeData.endcoords[1]), longitude: CLLocationDegrees(rideDetails.routeData.endcoords[0]))
        }
       
        
        if tripstatus == "1" || tripstatus == "2"{
            self.setPolyLineWithMaker(pickupaddr: "", dropaddr: "", pickupLoc: driverLoc, dropLoc: startLoc, tripSttaus: tripstatus)
        }else if tripstatus == "3"{
            self.setPolyLineWithMaker(pickupaddr: "", dropaddr: "", pickupLoc: driverLoc, dropLoc: endLoc, tripSttaus: tripstatus)
        }
    }
    
    func cancelRide(tripId : String){
        self.homevm.cancelRide(view: self.view, tripId: tripId)
        
        self.homevm.getcancelClouser = {
           self.updateTripSttaus(Status: "5")
            self.rideStatusView.deInitView()
        }
    }
}


//FirebaseDatas
extension HomeVC{
    
    func getFBCancelLationDetails(){
        self.FBConnect.cancelReason { (canceldatas) in
            Constant.lowbalnce = canceldatas.alertLabels.lowBalanceAlert ?? "0.0"
            Constant.miniBlncAlrt = canceldatas.alertLabels.minimumBalanceDriverAlert
        }
    }
    
    func getFBDriverDetails(){
        self.FBConnect.getdDriversData { (driverData) in
            if let drivers : FBDriverDataModel? = driverData{
                Constant.credits = drivers?.credits ?? "0.0"
                
//                let credits : Double = Double(Constant.credits) as? Double ?? 0.0
//                let lowblnc : Double = Double(Constant.lowbalnce) as? Double ?? 0.0
//                if credits <= lowblnc{
//                    self.setOnlinOffLine(online: "0")
//
//                }
                
                if drivers?.proof_status != "Accepted"{
                    self.proofStatusView.isHidden = false
                    if  drivers?.proof_status == "Rejected"{
                         self.proofstatusLbl.text = self.Localize.stringForKey(key: "proof_rej")
                        
                    }else if drivers?.proof_status != "Pending"{
                        self.proofstatusLbl.text = self.Localize.stringForKey(key: "proof_pending")
                    }else{
                        self.proofstatusLbl.text = drivers?.proof_status.lowercased()
                    }
                    self.setOnlinOffLine(online: "0")
                   self.startView.isUserInteractionEnabled = false
                    
                }else{
                    self.proofStatusView.isHidden = true
                   self.startView.isUserInteractionEnabled = true
                }
            }
            
            if let rideRequest : FBDriverDataModel? = driverData{
                if let trip_id : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? "" {
                    if trip_id.isEmpty {
                        if rideRequest?.request.status == "1"{
                            UserDefaults.standard.set("true", forKey: UserDefaultsKey.isacceptedView)
                                                     
                            let vc = AcceptDeclineVC.initWithStory()
                            vc.requestType = rideRequest?.request.request_type ?? ""
                             vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                        }
                        if rideRequest?.request.status == "2"{
                             UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
                            let tripid : String = rideRequest?.accept.trip_id ?? ""
                            UserDefaults.standard.set(tripid, forKey: UserDefaultsKey.tripId)
                        }
                    }else{
                         UserDefaults.standard.set("false", forKey: UserDefaultsKey.isacceptedView)
//                        if rideRequest?.request.status == "0"{
//                        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripId)
//                        }
                    }
                }
            }
        }
    }
    
    func updateFBLocation(loc : CLLocation){
        if let trip_id : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? "" {
                 if !trip_id.isEmpty{
                      self.FBConnect.updateGeoFire(loc: loc)
                 }else{
                      self.FBConnect.updateVehicleLocation(loc: loc)
                 }
                calculateTravelDis(loc: loc)
             }else{
                  self.FBConnect.updateVehicleLocation(loc: loc)
             }
    }
    
         func calculateTravelDis(loc : CLLocation){
            let trip_id : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? ""
            let isstarted : String = UserDefaults.standard.value(forKey: UserDefaultsKey.isstarted) as? String ?? ""
                
                   if !trip_id.isEmpty{
                            if isstarted == "true"{
                                
                                var speed : CLLocationSpeed = CLLocationSpeed()
                                speed = loc.speed
                                
                                if startlocations.coordinate.latitude == 0.0{
                                    self.startlocations = loc
                                    let distance : CLLocationDistance = loc.distance(from: self.startlocations)
                                    
                                    actualtravelDistance += Float(Float(distance)*( 1.04 / 1000.00))
    //                                showToast(msg: "Rider###Started@@@@@@\(String(format: "%.0f km/h", speed*3.6))====\((String(format: "%.3f", actualtravelDistance)))")
                                }else{
                                    let distance : CLLocationDistance = loc.distance(from: self.startlocations)
                                    
                                    self.startlocations = loc
                                    actualtravelDistance += Float(Float(distance)*( 1.04 / 1000.00))
    //                                showToast(msg: "RiderStarted@@@@@@\(String(format: "%.0f km/h", speed*3.6))=====\( (String(format: "%.3f", actualtravelDistance)))")
                                }
                                
                               
                            }
                     }
            }
    
    func removeFBLocation(){
        self.FBConnect.removerGeoFrie()
    }
    
    func onlineOfflineStatus(Status: String){
        self.FBConnect.setOnlineStatus(Status: Status)
    }
    
    func updateTripSttaus(Status: String){
        self.FBConnect.CreatestartFirebaseTripData(status: Status)
    }
   
    func updateEndTrip(rideDetails: TripStatusModel){
//        self.FBConnect.UpdateEndTripData(status: "4",rideDetails: rideDetails)
    }
   
    func getFBTripData(){
        self.FBConnect.getTripData { (tripData) in
           
            if let tripstatus : FBTripDataModel = tripData{
                if tripstatus.status == "1" || tripstatus.status == "2" || tripstatus.status == "3"{
                   
                    self.updateTripStatus(location: self.currentLocation, address: self.currentAddress, status: tripstatus.status, tripstatus: tripstatus,isButtonclick: false)
                    
                }else if tripstatus.status == "4"{
                    self.rideStatusView.deInitView()
                    self.rideDetailView.deInitView()
                    self.mapView.clear()
                    self.viewDidLoad()
                    let vc = InvoiceVC.initWithStory()
                     vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
                else if tripstatus.status == "5"{
                    
                    self.FBConnect.cancelDeclineEndTrip()
                    self.mapView.clear()
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripId)
                    self.rideStatusView.deInitView()
                    self.rideDetailView.deInitView()
                    self.otpView.deInitView()
                    self.vehicleView.deInitView()
                    
                }
            }
        }
    }
    
    func RouteRiderOTPViews(tripstatus : FBTripDataModel){
        
        self.rideStatusView.initView(view: self.view,address : self.currentAddress,tripRotue: self.tripRouteStatus, tripstatus: tripstatus, tripStatus: { (tripstatuss) in
            
            if tripstatuss == "2"{
                self.updateTripStatus(location: self.currentLocation, address: self.currentAddress, status: "2", tripstatus: tripstatus,isButtonclick: false)
                
            }else if tripstatuss == "3"{
                
                if let rideDetail : RideDetailView = self.rideDetailView{
                    self.rideDetailView.deInitView()
                }
                self.otpView.initView(view: self.view, pageFrom: "homepage",tripRotue: self.tripRouteStatus, submit: { (otp) in
                    if  !self.tripEndData.isEmpty{
                        self.getTravelDistance(pickupaddr: "", dropaddr: self.currentAddress, pickupLoc: CLLocation(), dropLoc: self.currentLocation, tripSttaus: "4" , tripstatus: tripstatus)
                    }else{
                        self.updateTripStatus(location: self.currentLocation, address: self.currentAddress, status: "3", tripstatus: tripstatus,isButtonclick: true)
                        self.otpView.deInitView()
                    }
                })
            }else if tripstatuss == "4"{
                self.tripEndData = "4"
                self.getTravelDistance(pickupaddr: "", dropaddr: self.currentAddress, pickupLoc: CLLocation(), dropLoc: self.currentLocation, tripSttaus: "4" , tripstatus: tripstatus)
              //  self.updateTripStatus(location: self.currentLocation, address: self.currentAddress, status: "4", tripstatus: tripstatus)
            }
        }, ridedetail: { (riderDetail) in
            
            self.rideDetailView.initView(view: self.view, tripRotue: self.tripRouteStatus, call: { (call) in
                if let url = URL(string: "tel://\(call)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }, message: { (message) in
                if (MFMessageComposeViewController.canSendText()) {
                    let controller = MFMessageComposeViewController()
                    controller.body = ""
                    controller.recipients = [message]
                    controller.messageComposeDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }
            }, cancel: { (cancel) in
                
                let tripid = UserDefaults.standard.value(forKey: UserDefaultsKey.tripId) as? String ?? ""
                
                self.cancelRide(tripId: tripid)
            })
            
        }, navigation: { (navigation) in
            
            if let route : TripRotemodel = self.tripRouteStatus.routeData {
              if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")! as URL)) {
                UIApplication.shared.openURL(NSURL(string:
                  "comgooglemaps://?saddr=\(route.startcoords[1]),\(route.startcoords[0])&daddr=\(route.endcoords[1]),\(route.endcoords[0])&directionsmode=driving")! as URL)
              } else {
                UIApplication.shared.openURL(NSURL(string:
                  "https://www.google.co.in/maps/dir/?saddr=\(route.startcoords[1]),\(route.startcoords[0])&daddr=\(route.endcoords[1]),\(route.endcoords[0])&directionsmode=driving")! as URL)
                NSLog("Can't use comgooglemaps://");
              }
            }

        })
    }
    
}

