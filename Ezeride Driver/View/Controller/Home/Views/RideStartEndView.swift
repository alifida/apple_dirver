//
//  RideStartEndView.swift
//  RebuStar Driver
//
//  Created by Abservetech on 07/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation


class RideStartEndView : UIView{
    
    @IBOutlet weak var RideView: UIView!
    @IBOutlet weak var detailView: UIView!
    
    @IBOutlet weak var rideDetailView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var navigationLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var tapBtn: UIButton!
    
    //variable declraction
    let Localize : Localizations = Localizations.instance
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initView(view : UIView ,address : String ,tripRotue: TripStatusModel,tripstatus : FBTripDataModel, tripStatus : @escaping(String) -> (), ridedetail : @escaping(String)->() , navigation : @escaping(String) -> ()){
        self.setView(view: view)
        self.setupLang()
        self.setupData(address: address, tripstatus: tripstatus,tripRotue: tripRotue)
        
        self.rideDetailView.addAction(for: .tap) {
            ridedetail("ridedetail_clicked")
        }
        
        self.navigationView.addAction(for: .tap) {
            navigation("navigation_clicked")
        }
        self.tapBtn.addAction(for: .tap) {
            if self.tapBtn.title(for: .normal) == self.Localize.stringForKey(key: "tap_arrive"){
//                self.tapBtn.setTitle(self.Localize.stringForKey(key: "tap_start"), for: .normal)
                tripStatus("2")
            }else if self.tapBtn.title(for: .normal) == self.Localize.stringForKey(key: "tap_start"){
//                self.tapBtn.setTitle(self.Localize.stringForKey(key: "tap_end"), for: .normal)
                tripStatus("3")
            }else if self.tapBtn.title(for: .normal) == self.Localize.stringForKey(key: "tap_end"){
                tripStatus("4")
               
                
            }
        }
        
    }
    
    func setupData(address : String ,tripstatus : FBTripDataModel,tripRotue: TripStatusModel){
        if let tripdata : FBTripDataModel = tripstatus{
            switch tripRotue.status{
            case  "Accept" :
                self.tapBtn.setTitle(self.Localize.stringForKey(key: "tap_arrive"), for: .normal)
                self.addressLbl.text = address
                break
            case "Arrive Now":
                self.tapBtn.setTitle(self.Localize.stringForKey(key: "tap_start"), for: .normal)
                self.addressLbl.text = tripRotue.routeData.start
                break
            case "Start Trip":
                self.tapBtn.setTitle(self.Localize.stringForKey(key: "tap_end"), for: .normal)
                self.addressLbl.text = tripRotue.routeData.end
                break
            default :
                break
            }
            
        }
    }
    
    //MARK: setView Property
    func setView(view : UIView) {
        //View Animation
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            
            self.frame = CGRect(x: 0,
                                y: view.frame.height * 0.67 ,
                                width: view.frame.width,
                                height: view.frame.height * 0.33)
            self.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
            view.addSubview(self)
            view.bringSubviewToFront(self)
        }
        
        self.transform = CGAffineTransform(translationX: 0, y: view.frame.height)//.concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
        
        UIView.animate(withDuration: 0.5) {
            self.transform = .identity
        }
        self.leftRightRoundCorners(radius: 15.0)
        self.tapBtn.roundeCornorBorderAppcolor = 5
        
    }
    
    
    func setupLang(){
        self.detailLbl.text = Localize.stringForKey(key: "rider_details")
        self.navigationLbl.text = Localize.stringForKey(key: "navigation")
        self.tapBtn.setTitle(Localize.stringForKey(key: "tap_arrive"), for: .normal)
    }
    
    func setData(){
        
    }
    
    //Mark : Removw view from parent view
    func deInitView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)//.concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    //MARK: Register xib view
    class var getView : RideStartEndView {
        return UINib(nibName: "RideStartEndView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RideStartEndView
    }
}
