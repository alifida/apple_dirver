//
//  BankDetailsVC.swift
//  Dash Driver
//
//  Created by Abservetech on 01/08/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit

class BankDetailsVC: UIViewController {

    @IBOutlet weak var bankDetails: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }

        setupView()

    }
    
    func setupView(){
        bankDetails.loadRequest(NSURLRequest(url: NSURL(string: ServiceApi.bankDetails)! as URL) as URLRequest)
    }
    
    class func initWithStory()->BankDetailsVC{
        let vc = UIStoryboard.init(name: "Payment", bundle: Bundle.main).instantiateViewController(withIdentifier: "BankDetailsVC") as! BankDetailsVC
        return vc
    }
    
}
