//
//  PaymentVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 31/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import Lottie
import SkyFloatingLabelTextField


class PaymentVC: UIViewController {

    @IBOutlet weak var lottieVIew: UIView!
    
    @IBOutlet weak var availableBln: UILabel!
    
    @IBOutlet weak var payoutBtn: UIButton!
    @IBOutlet weak var bankBtn: UIButton!
    
    @IBOutlet weak var payoutMainView: UIView!
    @IBOutlet weak var payoutSubview: UIView!
    @IBOutlet weak var payoutDismissView: UIView!
    
    @IBOutlet weak var payoutAmount: SkyFloatingLabelTextField!
    
    @IBOutlet weak var payoutSubmitBtn: UIButton!
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    
    var animationViewLarge = AnimationView()
    
    var payoutVM = CommonVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        payoutVM = CommonVM(dataService: ApiRoot())
        
        self.setupView()
        self.setupAction()
        self.setupLang()
        self.setupData()
    }
    
    func setupView(){
        self.barButtonItem(ViewController: self, title: Localize.stringForKey(key: "driver_credit"))

        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.revealViewController().rearViewRevealWidth = 300
        
        self.animationViewLarge = AnimationView(name: "wallet")
        let starbuildingAnimation = Animation.named("wallet")
        self.animationViewLarge.animation = starbuildingAnimation
        self.animationViewLarge.animationSpeed = 0.2
        self.animationViewLarge.frame = CGRect(x: 0, y: -20, width: self.lottieVIew.frame.width, height: self.lottieVIew.frame.height)
        self.lottieVIew.addSubview(self.animationViewLarge)
        self.lottieVIew.clipsToBounds = true
        self.lottieVIew.layer.masksToBounds = true
        
        UIView.animate(withDuration: 2.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.animationViewLarge.play()
        }, completion: nil)
        
    }
    func setupAction(){
        payoutBtn.addAction(for: closureActions.tap) {
            self.payoutMainView.isHidden = false
        }
        payoutDismissView.addAction(for: closureActions.tap) {
            self.payoutMainView.isHidden = true
        }
        
        payoutSubmitBtn.addAction(for: .tap) {
            self.payoutAttemptApi()
        }
        bankBtn.addAction(for: .tap) {
            var bankDetailsVC = BankDetailsVC.initWithStory()
            self.navigationController?.pushViewController(bankDetailsVC, animated: true)
        }
    }
    func setupLang(){
        
    }
    
    func setupData(){
        self.availableBln.text = "\(Localize.stringForKey(key: "avail_bal")) " + Constant.priceTag + Constant.credits
    }
   
    class func initWithStory()->PaymentVC{
        let vc = UIStoryboard.init(name: "Payment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        return vc
    }
    
    func payoutAttemptApi(){
        payoutVM.payoutApi(view: self.view, amount: payoutAmount.text ?? "")
        payoutVM.successPayoutClosure = {
             self.payoutMainView.isHidden = true
        }
    }
}
