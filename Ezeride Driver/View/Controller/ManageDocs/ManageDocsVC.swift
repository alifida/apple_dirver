//
//  ManageDocsVC.swift
//  RebuStar Driver
//
//  Created by Abservetech on 05/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import PINRemoteImage

class ManageDocsVC: UIViewController {

    //UI Declaraction
    @IBOutlet weak var documentTabelView: UITableView!
    
    @IBOutlet weak var docsView: UIView!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var selectedIndex : Int = -1
    var docType : String = "user" //user , vehicle
    var pageFrom : String = ""
    var docArray :  [String] = []
    var vehiclePosition : Int = Int()
    var profile = ProfileVM()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        if self.pageFrom == "signup"{
            self.navigationController?.isNavigationBarHidden = true
        }
        self.profile = ProfileVM(view: self.view, dataService: ApiRoot())
            self.profile.getProfile()
            self.selectedIndex = -1
            self.profile.successprofile = {
                self.setupData()
            }
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.setupView()
        self.setupAction()
        self.setupLang()
        self.setupTabelView()
        self.setupData()
    }
    
    func setupView(){
        if docType == "vehicle"{
            self.title = Localize.stringForKey(key: "manage_docs")
        }else{
            self.barButtonItem(ViewController: self, title: Localize.stringForKey(key: "manage_docs"))
        }
        if self.pageFrom == "signup"{
            self.navigationItem.leftBarButtonItem = nil
            self.docsView.isHidden = false
        }else{
           self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
            self.revealViewController().rearViewRevealWidth = 300
             self.docsView.isHidden = true
        }
        
       
        
    }
    
    func setupAction(){
        self.nextBtn.addAction(for: .tap) {
            
            self.navigationController?.isNavigationBarHidden = false
                let vc = AddVechileVC.initWithStory()
                vc.pageFrom = self.pageFrom
                self.navigationController?.pushViewController(vc, animated: true)
        }
        self.skipBtn.addAction(for: .tap) {
            
            self.navigationController?.isNavigationBarHidden = false
            let vc = AddVechileVC.initWithStory()
            vc.pageFrom = self.pageFrom
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func setupLang(){
        if docType == "vehicle"{
            self.docArray = [Localize.stringForKey(key: "taxi_passing"),Localize.stringForKey(key: "permit"),Localize.stringForKey(key: "rc")]
            
        }else{
            self.docArray = [Localize.stringForKey(key: "driving_license"),Localize.stringForKey(key: "insurance")]
        }
    }
    
    func setupData(){
        if docType != "vehicle"{
            if let profile : ProfileModel = Constant.profileData{
                self.documentTabelView.reloadData()
            }
        }else{
            if let profile : Taxis = Constant.profileData.taxis[self.vehiclePosition]{
                self.documentTabelView.reloadData()
            }
        }
    }
    
    class func initWithStory()->ManageDocsVC{
        let vc = UIStoryboard.init(name: "ManageDocs", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageDocsVC") as! ManageDocsVC
        return vc
    }

}



extension ManageDocsVC : UITableViewDataSource,UITableViewDelegate{
    
    func setupTabelView(){
        self.documentTabelView.register(UINib(nibName: "DocumentCell", bundle: nil), forCellReuseIdentifier: "DocumentCell")
        
        self.documentTabelView.delegate = self
        self.documentTabelView.dataSource = self
        self.documentTabelView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.docArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath) as! DocumentCell
        cell.doc_title.text = docArray[indexPath.row]
        
        if selectedIndex == indexPath.row{
            if cell.headerBottomView.isHidden{
                cell.headerBottomView.isHidden = false
                cell.downArrowImg.image = UIImage(named: "up-arrow")
            }else{
                cell.headerBottomView.isHidden = true
                cell.downArrowImg.image = UIImage(named: "down-arrow")
            }
        }else{
            cell.headerBottomView.isHidden = true
            cell.downArrowImg.image = UIImage(named: "down-arrow")
        }
      
        if docType != "vehicle"{
            if let profile : ProfileModel = Constant.profileData{
              print("profileDocmentDatas",profile.licence , profile.insurance)
                if indexPath.row == 0 {
                    if !profile.licence.isEmpty{
                        cell.missingLbl.isHidden = true
                        cell.infoImg.isHidden = true
                        cell.manageDocView.isHidden = false
                        cell.uploadDocView.isHidden = true
                        var urls : String = ServiceApi.Base_Image_URL+profile.licence ?? String()
                        cell.docImg.pin_setImage(from: URL(string: urls))
                    }else{
                        cell.missingLbl.isHidden = false
                        cell.infoImg.isHidden = false
                        cell.manageDocView.isHidden = true
                        cell.uploadDocView.isHidden = false
                    }
                }else   if indexPath.row == 1 {
                    if !profile.insurance.isEmpty{
                        cell.missingLbl.isHidden = true
                        cell.infoImg.isHidden = true
                        cell.manageDocView.isHidden = false
                        cell.uploadDocView.isHidden = true
                        var urls : String = ServiceApi.Base_Image_URL+profile.insurance ?? String()
                        cell.docImg.pin_setImage(from: URL(string: urls))
                    }else{
                        cell.missingLbl.isHidden = false
                        cell.infoImg.isHidden = false
                        cell.manageDocView.isHidden = true
                        cell.uploadDocView.isHidden = false
                    }
                }
            }
        }else{
            if let profile : Taxis = Constant.profileData.taxis[vehiclePosition]{
                if indexPath.row == 0 {
                    if !profile.insurance.isEmpty{
                        cell.missingLbl.isHidden = true
                        cell.manageDocView.isHidden = false
                        cell.uploadDocView.isHidden = true
                        var urls : String = ServiceApi.Base_Image_URL+profile.insurance ?? String()
                        cell.docImg.pin_setImage(from: URL(string: urls))
                    }else{
                        cell.missingLbl.isHidden = false
                        cell.manageDocView.isHidden = true
                        cell.uploadDocView.isHidden = false
                    }
                }else   if indexPath.row == 1 {
                    if !profile.permit.isEmpty{
                        cell.missingLbl.isHidden = true
                        cell.manageDocView.isHidden = false
                        cell.uploadDocView.isHidden = true
                        var urls : String = ServiceApi.Base_Image_URL+profile.permit ?? String()
                        cell.docImg.pin_setImage(from: URL(string: urls))
                    }else{
                        cell.missingLbl.isHidden = false
                        cell.manageDocView.isHidden = true
                        cell.uploadDocView.isHidden = false
                    }
                }else   if indexPath.row == 2 {
                    if !profile.registration.isEmpty{
                        cell.missingLbl.isHidden = true
                        cell.manageDocView.isHidden = false
                        cell.uploadDocView.isHidden = true
                        var urls : String = ServiceApi.Base_Image_URL+profile.registration ?? String()
                        cell.docImg.pin_setImage(from: URL(string: urls))
                    }else{
                        cell.missingLbl.isHidden = false
                        cell.manageDocView.isHidden = true
                        cell.uploadDocView.isHidden = false
                    }
                }
            }
        }
        cell.docHeaderView.addAction(for: .tap) {
            if cell.headerBottomView.isHidden{
                self.selectedIndex = indexPath.row
            }else{
                self.selectedIndex = -1
            }
            self.documentTabelView.reloadData()
        }
        
        
        cell.manageDocBtn.addAction(for: .tap) {
            if self.docType != "vehicle"{
                let vc = UploadDocVC.initWithStory()
                vc.imagetopost = cell.docImg.image ?? UIImage()
                if indexPath.row == 0 {
                    vc.docType = "licence"
                    Constant.filefor = "licence"
                }else{
                    vc.docType = "aadhar"
                    Constant.filefor = "insurance"
                }
                vc.pageFor = "manage"
                vc.pageofVehcileOrUser = "user"
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                let vc = UploadDocVC.initWithStory()
                vc.imagetopost = cell.docImg.image ?? UIImage()
                if indexPath.row == 0 {
                    vc.docType = "insurance"
                    Constant.filefor = "Insurance"
                }else if indexPath.row == 1 {
                    vc.docType = "permit"
                    Constant.filefor = "permit"
                }else if indexPath.row == 2 {
                    vc.docType = "registration"
                    Constant.filefor = "registration"
                }
                vc.pageFor = "manage"
                vc.pageofVehcileOrUser = "vehicle"
                vc.vehiclePosition = self.vehiclePosition
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        cell.uploadDocBtn.addAction(for: .tap) {
            if self.docType != "vehicle"{
                 let vc = UploadDocVC.initWithStory()
                vc.pageFor = "upload"
                vc.pageofVehcileOrUser = "user"
                if indexPath.row == 0 {
                    vc.docType = "licence"
                    Constant.filefor = "licence"
                }else{
                    vc.docType = "aadhar"
                    Constant.filefor = "insurance"
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = UploadDocVC.initWithStory()
                vc.pageFor = "upload"
                vc.pageofVehcileOrUser = "vehicle"
                if indexPath.row == 0 {
                    vc.docType = "insurance"
                    Constant.filefor = "Insurance"
                }else if indexPath.row == 1 {
                    vc.docType = "permit"
                    Constant.filefor = "permit"
                }else if indexPath.row == 2 {
                    vc.docType = "registration"
                    Constant.filefor = "registration"
                }
                vc.vehiclePosition = self.vehiclePosition
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == indexPath.row{
            return 150
        }else{
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! DocumentCell
        cell.backgroundColor = UIColor.clear
        
        
    }
}


