//
//  UploadDocVC.swift
//  RebuStar Driver
//
//  Created by Abservetech on 06/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class UploadDocVC: UIViewController {

    @IBOutlet weak var docImg: UIImageView!
    @IBOutlet weak var datepickerview: UIView!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var selectDocLbl: UILabel!
    @IBOutlet weak var expireDateTXF: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var expiryBtn: UIButton!
   
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var pageFor : String = ""
    var docType : String = ""
    var pageofVehcileOrUser : String = ""
    var vehiclePosition : Int = Int()
    var pickImage : UIImagePickerController? =  UIImagePickerController()
    var imagetopost = UIImage()
    var editImageToUpload = UIImage()
    
    var vehicleVM = ManageVehicleVM()
    var makeid : String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.vehicleVM = ManageVehicleVM(dataService: ApiRoot())
        self.setupView()
        self.setupAction()
        self.setupLang()
        self.setupData()
        self.setupPickerDelegate()
    }
    
    func setupView(){
        self.barButtonItem(ViewController: self, title: Localize.stringForKey(key: "upload_docs"))
        self.navigationItem.leftBarButtonItem = nil
        self.expireDateTXF.withImage(direction: .Right, image: UIImage(named: "down-arrow") ?? UIImage(), colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
    }
    
    @IBAction func datePickerDoneBtn(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        datepicker.minimumDate = Date()
        expireDateTXF.text = dateFormatter.string(from: datepicker.date)
        self.datepickerview.isHidden = true
        
    }
    
    func setupAction(){
        self.expiryBtn.addAction(for: .tap) {
            self.datepickerview.isHidden = false
        }
        
        self.docImg.addAction(for: .tap) {
            self.pickProfileImage()
        }
        
        self.submitBtn.addAction(for: .tap) {
            if !Constant.filefor.isEmpty{
                if !(self.expireDateTXF.text?.isEmpty ?? false){
                    let driverid = UserDefaults.standard.string(forKey: UserDefaultsKey.userid) as? String ?? ""
                    if self.pageofVehcileOrUser == "user"{
                         self.uploadDocs(image: self.imagetopost, filefor: Constant.filefor, driverid: driverid, licenceexp: self.expireDateTXF.text ?? "")
                    }else{
                        self.imagetopost = self.docImg.image ?? UIImage()
                        
                        self.uploadvehicleDocs(image: self.imagetopost, filefor: Constant.filefor, driverid: driverid, licenceexp: self.expireDateTXF.text ?? "", makeid: Constant.profileData.taxis[self.vehiclePosition]._id ?? "", type: "")
                    }
                   
                }else{
                    showToast(msg: "Expire date must not be Empty")
                }
            }else{
                showToast(msg: "Choose File type for upload document")
            }
        }
    }
    func setupLang(){
        self.selectDocLbl.text = Localize.stringForKey(key: "select_doc")
        self.submitBtn.setTitle(Localize.stringForKey(key: "submit"), for: .normal)
    }
    
    func setupData(){
        if pageFor == "manage"{
            if pageofVehcileOrUser == "user"{
                if let profile : ProfileModel = Constant.profileData{
                    if docType == "licence" {
                        if !profile.licence.isEmpty{
                            self.docImg.image = UIImage(named: "document_uploaded")
                            self.expireDateTXF.text = profile.licenceexp
                        }
                    }else if docType == "aadhar" {
                         if !profile.insurance.isEmpty{
                            self.docImg.image = UIImage(named: "document_uploaded")
                            self.expireDateTXF.text = profile.insuranceexp
                        }
                    }
                }
            }else{
                if let profile : Taxis = Constant.profileData.taxis[vehiclePosition]{
                    self.makeid = profile._id
                    if docType == "insurance" {
                        if !profile.licence.isEmpty{
                            self.docImg.image = UIImage(named: "document_uploaded")
                            self.expireDateTXF.text = profile.insuranceexpdate
                        }
                    }else if docType == "permit" {
                        if !profile.insurance.isEmpty{
                            self.docImg.image = UIImage(named: "document_uploaded")
                            self.expireDateTXF.text = profile.permitexpdate
                        }
                    }else if docType == "registration" {
                        if !profile.insurance.isEmpty{
                            self.docImg.image = UIImage(named: "document_uploaded")
                            self.expireDateTXF.text = profile.registrationexpdate
                        }
                    }
                }
            }
        }else if pageFor == "upload"{
            self.docImg.image = UIImage(named: "document_upload")
            self.expireDateTXF.text = ""
        }
    }
    
    class func initWithStory()->UploadDocVC{
        let vc = UIStoryboard.init(name: "ManageDocs", bundle: Bundle.main).instantiateViewController(withIdentifier: "UploadDocVC") as! UploadDocVC
        return vc
    }

}

//Image Picker
extension UploadDocVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func setupPickerDelegate(){
        pickImage?.delegate = self
        pickImage?.allowsEditing = true
    }
    
    func pickProfileImage(){
        
        let imageEdit = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        imageEdit.modalPresentationStyle = .popover
        imageEdit.addAction(UIAlertAction(title: Localize.stringForKey(key: "take_photo"), style: .default, handler: { (photo) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                DispatchQueue.main.async(execute: {
                    self.pickImage?.sourceType = UIImagePickerController.SourceType.camera
                    self.pickImage?.mediaTypes = ["public.image"]
                    if UIImagePickerController.isCameraDeviceAvailable(.front) {
                        self.pickImage?.cameraDevice = .front
                    }
                    else {
                        self.pickImage?.cameraDevice = .rear
                    }
                    self.present(self.pickImage!, animated: true, completion: nil)
                })
            }
        }))
        imageEdit.addAction(UIAlertAction(title: Localize.stringForKey(key: "choose_gallery"), style: .default, handler: { (photo) in
            self.pickImage?.allowsEditing = true
            self.pickImage?.sourceType = .photoLibrary
            self.present(self.pickImage!, animated: true, completion: nil)
        }))
        imageEdit.addAction(UIAlertAction(title: Localize.stringForKey(key: "cancel"), style: .cancel, handler: { (_ ) in
        }))
        
        if let popview = imageEdit.popoverPresentationController{
            popview.sourceView = self.docImg
            popview.sourceRect = self.docImg.bounds
        }
        self.present(imageEdit, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        pickImage?.dismiss(animated: true, completion: nil)
        var imagetoupload :UIImage?
        
        if let image = info[.originalImage] as? UIImage {
            imagetoupload = image
        }
        
        if let image = info[.editedImage] as? UIImage {
            imagetoupload = image
        }
        
        if imagetoupload != nil {
            self.docImg.image = imagetoupload
            imagetopost = imagetoupload!
        }
    }
}

extension UploadDocVC{
    func uploadDocs(image : UIImage , filefor : String , driverid : String , licenceexp : String){
        self.vehicleVM.editDoucs(view: self.view, image: image, filefor: filefor, driverid: driverid, licenceexp: licenceexp, dataSuccess: {(success) in
            self.navigationController?.popViewController(animated: true)
        
        })
        self.vehicleVM.setuploadedClosure? = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func uploadvehicleDocs(image : UIImage , filefor : String , driverid : String , licenceexp : String , makeid : String , type : String){
        self.vehicleVM.drivertaxiDoc(view: self.view, image: image, filefor: filefor, driverid: driverid, licenceexp: licenceexp,makeid : makeid, type: type, dataSuccess: {(success) in
            self.navigationController?.popViewController(animated: true)
            
        })
        self.vehicleVM.setuploadedClosure? = {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
