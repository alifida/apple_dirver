//
//  VechileServeicModel.swift
//  RebuStar Rider
//
//  Created by Abservetech on 28/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation
import SwiftyJSON

struct onlineModel {
    var success : String = String()
    var message : String = String()
 
    init(json : JSON){
        self.success = json["success"].string ?? String()
        self.message = json["message"].string ?? String()
    }
}


struct TripAcceptDeclineModel {
    var eta : String = String()
    var message : String = String()
    var requestId : String = String()
    var success : Bool = Bool()
    var tripId : Int = Int()
    init(){}
    init(json : JSON){
        self.eta = json["eta"].string ?? String()
        self.message = json["message"].string ?? String()
        self.requestId = json["requestId"].string ?? String()
        self.success = json["success"].bool ?? Bool()
        self.tripId = json["tripId"].int ?? Int()
    }
}


struct TripStatusModel {
    var status : String = String()
    var message : String = String()
    var success : Bool = Bool()
    var startOTP : String = String()
    var taxitype : String = String()
    var endOTP : String = String()
    var rider : riderModel = riderModel()
    var routeData : TripRotemodel = TripRotemodel()
    init(){}
    init(json : JSON){
        self.status = json["status"].string ?? String()
        self.message = json["message"].string ?? String()
        self.success = json["success"].bool ?? Bool()
        self.startOTP = json["startOTP"].string ?? String()
        self.taxitype = json["taxitype"].string ?? String()
        self.endOTP = json["endOTP"].string ?? String()
        self.rider = riderModel.init(json: json["rider"])
        self.routeData = TripRotemodel.init(json: json["pickupdetails"])
    }
}

struct riderModel {
    var id : String = String()
    var cur : String = String()
    var lang : String = String()
    var cntyname : String = String()
    var phone : String = String()
    var phcode : String = String()
    var email : String = String()
    var lname : String = String()
    var fname : String = String()
    var profileurl : String = String()
    var points : String = String()
    var fcmId : String = String()
    var balance : String = String()
    var othersPhone : String = String()
    var otherName : String = String()
    var notes : String = String()
    init(){}
    init(json : JSON){
        self.id = json["id"].string ?? String()
        self.cur = json["cur"].string ?? String()
        self.lang = json["lang"].string ?? String()
        self.cntyname = json["cntyname"].string ?? String()
        self.phone = json["phone"].string ?? String()
        self.phcode = json["phcode"].string ?? String()
        self.email = json["email"].string ?? String()
        self.lname = json["lname"].string ?? String()
        self.fname = json["fname"].string ?? String()
        self.profileurl = json["profileurl"].string ?? String()
        self.points = json["points"].string ?? String()
        self.fcmId = json["fcmId"].string ?? String()
        self.balance = json["balance"].string ?? String()
        self.othersPhone = json["othersPhone"].string ?? String()
        self.otherName = json["otherName"].string ?? String()
        self.notes = json["notes"].string ?? String()
    }
}

struct TripRotemodel {
    var end : String = String()
    var start : String = String()
    var distanceKM : String = String()
    var startcoords : [Float] = [Float]()
    var endcoords : [Float] = [Float]()
    init(){}
    init(json : JSON){
        self.end = json["end"].string ?? String()
        self.start = json["start"].string ?? String()
        self.distanceKM = json["distanceKM"].string ?? String()
        
        let startcoordsArray = json["startcoords"].array
        
        let startcoordsList = startcoordsArray?.compactMap({ (jsonVal) -> Float? in
            return jsonVal.float
        })
        self.startcoords = startcoordsList ?? [Float]()
        
        
        let endcoordsArray = json["endcoords"].array
        
        let endcoordsList = endcoordsArray?.compactMap({ (jsonVal) -> Float? in
            return jsonVal.float
        })
        self.endcoords = endcoordsList ?? [Float]()
        
    }
}

struct FeedBackModel {
    var success : Bool = Bool()
    var message : String = String()
    
    init(){}
    init(json : JSON) {
        self.success = json["success"].bool ?? Bool()
        self.message = json["message"].string ?? String()
    }
}
