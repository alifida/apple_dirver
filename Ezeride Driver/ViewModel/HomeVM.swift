//
//  HomeVM.swift
//  RebuStar Rider
//
//  Created by Abservetech on 28/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import GoogleMaps

class HomeVM{
    
    //Mark :- Data Declaraction
    var dataService : ApiRoot?
    
    var view : UIView?
    
    var error: Error? {
        didSet {
            guard let error = error else { return }
            self.showErrorAlertClosure?()
        }
    }
    
    var online : onlineModel?{
        didSet{
            guard let vechile = online else { return }
            self.getOnlineclouser?()
        }
    }
    var errOnline : onlineModel?{
        didSet{
            guard let error = errOnline else { return }
            showToast(msg: " Try again Later!!")
        }
    }
    
    var accept : TripAcceptDeclineModel?{
        didSet{
            guard let acceptdata = self.accept else { return }
            self.getacceptclouser?()
        }
    }
    var erraccept : TripAcceptDeclineModel?{
        didSet{
            guard let acceptdata = self.erraccept else { return }
            self.eracceptclouser?()
            showToast(msg: acceptdata.message )
        }
    }
    var decline : TripAcceptDeclineModel?{
        didSet{
            guard let acceptdata = self.decline else { return }
            self.getdeclineclouser?()
            
        }
    }
    var errdecline : TripAcceptDeclineModel?{
        didSet{
            guard let acceptdata = self.errdecline else { return }
            self.erdeclineclouser?()
            showToast(msg: acceptdata.message +  "Sorry!, Try again later!!!")
        }
    }
    
    var updateLocation : String?{
        didSet{
            guard let acceptdata = self.updateLocation else { return }
            self.getupdateLocclouser?()
            
        }
    }
    var errupdateLocation : String?{
        didSet{
            guard let acceptdata = self.errupdateLocation else { return }
            self.errupdateLocclouser?()
//            showToast(msg: "Sorry!, Try again later!!!")
        }
    }
    
    
    var tripRoute : TripStatusModel?{
        didSet{
            guard let triproute = tripRoute else {
                return
            }
            self.getTripRouteClouser?()
        }
    }
    
    var errtripRoute : TripStatusModel?{
        didSet{
            guard let triproute = errtripRoute else {
                return
            }
            self.errTripRouteClouder?()
            showToast(msg: triproute.message)
        }
    }
    
    var cancelTrip : TripStatusModel?{
        didSet{
            guard let triproute = cancelTrip else {
                return
            }
            self.getcancelClouser?()
        }
    }
    
    var errcancelTrip : TripStatusModel?{
        didSet{
            guard let triproute = errcancelTrip else {
                return
            }
            self.errcancelClouder?()
            showToast(msg: triproute.message)
        }
    }
    
    //Mark :- Constructor
    init() { }
    
    init(view : UIView ,dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    init(dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    var feedback : FeedBackModel?{
        didSet{
            guard let response = feedback else { return }
            getfeedbackClouser?()
        }
    }
    
    var errfeedback : FeedBackModel?{
        didSet{
            guard let err = errfeedback else { return }
            errfeedbackClouser?()
            showToast(msg: err.message ?? "")
        }
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var errorinFetchData: (() -> ())?
    var showErrorAlertClosure: (() -> ())?
    var getOnlineclouser  : (()->())?
    var errOnlineclouser : (() -> ())?
    var getacceptclouser  : (()->())?
    var eracceptclouser : (() -> ())?
    var getdeclineclouser  : (()->())?
    var erdeclineclouser : (() -> ())?
    var getupdateLocclouser  : (()->())?
    var errupdateLocclouser : (() -> ())?
    var getTripRouteClouser : (() -> ())?
    var errTripRouteClouder : (()->())?
    var getcancelClouser : (() -> ())?
    var errcancelClouder : (()->())?
    var getfeedbackClouser : (() -> ())?
    var errfeedbackClouser : (()->())?
  
    func changeOnlineStatus(view : UIView , status : String ){
        
        let url = ServiceApi.online
        
        var params = Parameters()
        params["status"] = status
       
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.online = onlineModel(json: success)
        }, jsonError: { (jsonError) in
            self.errOnline = onlineModel(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func acceptRide(view : UIView , requestId : String ){
        
        let url = ServiceApi.acceptRequest
        
        var params = Parameters()
        params["requestId"] = requestId
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.accept = TripAcceptDeclineModel.init(json : success)
        }, jsonError: { (jsonError) in
            self.erraccept = TripAcceptDeclineModel.init(json : jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func declineRequest(view : UIView , requestId : String ){
        
        let url = ServiceApi.declineRequest
        
        var params = Parameters()
        params["requestId"] = requestId
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.decline = TripAcceptDeclineModel.init(json : success)
        }, jsonError: { (jsonError) in
            self.errdecline = TripAcceptDeclineModel.init(json : jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func updateLocation(loc : CLLocation ,status : String){
        let url = ServiceApi.DriverLocation
        
        var params = Parameters()
        params["lat"] = loc.coordinate.latitude.description
        params["lon"] = loc.coordinate.longitude.description
        params["status"] = status
        
        self.dataService?.putLocApi( url: url, params: params, jsonSuccess: { (success) in
            self.updateLocation = success["message"].string ?? ""
        }, jsonError: { (jsonError) in
            self.errupdateLocation = jsonError["message"].string ?? ""
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    
    // update Ride locations
    func tripCurrentStatus(view : UIView ,allowanceDistance : String , pickupLat : String , pickupLng : String ,startTime : String ,fromAddress : String ,endAddress : String,endTime : String , waitingTime : String ,waitingSecond : String , additionalFee : String , dropLng : String , dropLat : String, duration : String , tripId : String , status : String,distance : String){
        let url = ServiceApi.tripCurrentStatus
        
        var params = Parameters()
        params["allowanceDistance"] = allowanceDistance
        params["pickupLat"] = pickupLat
        params["pickupLng"] = pickupLng
        params["startTime"] = startTime
        params["fromAddress"] = fromAddress
        params["endAddress"] = endAddress
        params["endTime"] = endTime
        params["waitingTime"] = waitingTime
        params["waitingSecond"] = waitingSecond
        params["additionalFee"] = additionalFee
        params["dropLng"] = dropLng
        params["dropLat"] = dropLat
        params["duration"] = duration
        params["tripId"] = tripId
        params["status"] = status
        params["distance"] = distance
        
       
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.tripRoute = TripStatusModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errtripRoute = TripStatusModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func cancelRide(view : UIView ,tripId : String){
        let url = ServiceApi.cancelTrip
        
        var params = Parameters()
        params["tripId"] = tripId
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.cancelTrip = TripStatusModel.init(json: success)
            
        }, jsonError: { (jsonError) in
            self.errcancelTrip = TripStatusModel.init(json: jsonError)
            
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func riderFeedBack(view : UIView , tripId : String,rating : String ,comments : String ){
        let url = ServiceApi.driverFeedback
        
        var params = Parameters()
        params["tripId"] = tripId
        params["rating"] = rating
        params["comments"] = comments
        
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.feedback = FeedBackModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errfeedback = FeedBackModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
        
    }
}



