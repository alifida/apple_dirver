
//
//  Earnings.swift
//  RebuStar Driver
//
//  Created by Abservetech on 09/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class EarningsVM{
    
    //Mark :- Data Declaraction
    var dataService : ApiRoot?
    
    var view : UIView?
    
    var error: Error? {
        didSet {
            guard let error = error else { return }
            self.showErrorAlertClosure?()
        }
    }
    
    var earningsList : EarningModel?{
        didSet{
            guard let vechile = earningsList else { return }
            self.getearningListClosure?()
        }
    }
    var errearningList : EarningModel?{
        didSet{
            guard let error = errearningList else { return }
            showToast(msg: "Try again Later!!")
        }
    }
    
    
    
    //Mark :- Constructor
    init() { }
    
    init(view : UIView ,dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    init(dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var errorinFetchData: (() -> ())?
    var showErrorAlertClosure: (() -> ())?
    var getearningListClosure: (()->())?
    var errearningListClosure : (() -> ())?
    
    
    // MARK: - Network call
    func getEaringsList(view : UIView,from : String , to : String , type : String , fromMonth : String , page : String ){
     
        let url = ServiceApi.driverEarnings
        
        var params = Parameters()
        params["from"] = from
        params["to"] = to
        params["type"] = type
        params["fromMonth"] = fromMonth
        params["_page"] = page
       
        self.dataService?.postApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.earningsList = EarningModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errearningList = EarningModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
}





