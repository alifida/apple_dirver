//
//  ProfileVM.swift
//  RebuStar Rider
//
//  Created by Abservetech on 12/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class ProfileVM{
    
    //Mark :- Data Declaraction
    var dataService : ApiRoot?
    
    var view : UIView?
    
    var error: Error? {
        didSet {
            guard let error = error else { return }
            self.showErrorAlertClosure?()
        }
    }
    
    var profileData : ProfileModel?{
        didSet {
            self.successprofile?()
        }
    }
    
    var changepass : changePasswprd?{
        didSet {
            self.successChangePAss?()
        }
    }
    var errchangepass : changePasswprd?{
        didSet {
            self.errorChangePAss?()
        }
    }
    
    var profileEdit : ProfileEditModel?{
        didSet{
            var profile = ProfileModel()
            profile.email = self.profileEdit?.request.email ?? ""
            profile.fname = self.profileEdit?.request.fname  ?? ""
            profile.lname = self.profileEdit?.request.lname  ?? ""
            profile.phone = self.profileEdit?.request.phone  ?? ""
            profile.phcode = self.profileEdit?.request.phcode  ?? ""
            profile.driverAddress = self.profileEdit?.request.driverAddress ?? ""
            profile.profile = self.profileEdit?.request.profile ?? ""
            self.profileData = profile
        }
    }
    
    var profileErr : ProfileModel?{
        didSet {
            self.errorprofile?()
        }
    }
    
    //Mark :- Constructor
    init() { }
    
    init(view : UIView ,dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    init(dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var errorinFetchData: (() -> ())?
    var showErrorAlertClosure: (() -> ())?
    var successprofile : (() -> ())?
    var errorprofile : (() -> ())?
    var successChangePAss : (() -> ())?
    var errorChangePAss : (() -> ())?
    
    // MARK: - Network call
    func getProfile(){
        let url = ServiceApi.profile
        
        var params = Parameters()
       
        self.dataService?.getApiwithoutView(url: url, params: params, jsonSuccess: { (success) in
            self.profileData = ProfileModel.init(json: success[0])
            Constant.profileData =  self.profileData ?? ProfileModel()
            
            Constant.currentTaxi = UserProfile.init(json: success[2]).currentActiveTaxi
             UserDefaults.standard.set(( UserProfile.init(json: success[2]).currentActiveTaxi.vehicletype ?? "").lowercased(), forKey: UserDefaultsKey.defaultVehicle)
        }, jsonError: { (jsonError) in
            self.profileErr = ProfileModel.init(json: jsonError[0])
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    // MARK: - Edit Profile With upload Images
    func editProfile(view : UIView , image : UIImage ,fname : String , lname : String ,email : String , phone : String , phcode : String,driverAddress: String){
        
        let url = ServiceApi.profileUpdate
        
        var params = Parameters()
        params["fname"] = fname
        params["lname"] = lname
        params["email"] = email
        params["phone"] = phone
        params["phcode"] = phcode
        params["address"] = driverAddress
        
        self.dataService?.fileUploadPostApi(view: view, url: url, params: params, image: image, jsonSuccess: { (success) in
            self.profileEdit = ProfileEditModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.profileErr = ProfileModel.init(json: jsonError[0])
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    // MARK: - Edit Profile With upload Images
    func changepassword(view : UIView ,oldpass : String , newPass : String , confirmPass : String){
        
        let url = ServiceApi.driverpwd
        
        var params = Parameters()
        params["oldpassword"] = oldpass
        params["newpassword"] = newPass
        params["confirmpassword"] = confirmPass
        
        self.dataService?.putApi(view: view, url: url, params: params,  jsonSuccess: { (success) in
           
            self.changepass = changePasswprd.init(json: success)
        }, jsonError: { (jsonError) in
             self.errchangepass = changePasswprd.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
}
